# Microsoft Azure DevOps

Vendor: Microsoft
Homepage: https://www.microsoft.com/en-us/

Product: Azure DevOps
Product Page: https://azure.microsoft.com/en-us/products/devops

## Introduction
We classify Microsoft Azure DevOps into the CI/CD domain since Microsoft Azure DevOps handles the versioning, building, storage and deployment. 

## Why Integrate
The Azure DevOps adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft Azure DevOps. With this adapter you have the ability to perform operations such as:

- Get Pipelines
- Get Pipeline
- Get Pipeline Runs
- Get Repositories
- Get Pull Requests
- Create Pull Request
- Commit

## Additional Product Documentation
The [API documents for Microsoft Azure DevOps](https://learn.microsoft.com/en-us/rest/api/azure/devops/?view=azure-devops-rest-7.2)