# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Azure_devops System. The API that was used to build the adapter for Azure_devops is usually available in the report directory of this adapter. The adapter utilizes the Azure_devops API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Azure DevOps adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft Azure DevOps. With this adapter you have the ability to perform operations such as:

- Get Pipelines
- Get Pipeline
- Get Pipeline Runs
- Get Repositories
- Get Pull Requests
- Create Pull Request
- Commit

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
