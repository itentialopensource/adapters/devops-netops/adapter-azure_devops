/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-azure_devops',
      type: 'AzureDevops',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const AzureDevops = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Azure_devops Adapter Test', () => {
  describe('AzureDevops Class Tests', () => {
    const a = new AzureDevops(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('azure_devops'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('azure_devops'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('AzureDevops', pronghornDotJson.export);
          assert.equal('Azure_devops', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-azure_devops', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('azure_devops'));
          assert.equal('AzureDevops', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-azure_devops', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-azure_devops', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#pipelinesCreate - errors', () => {
      it('should have a pipelinesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pipelinesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pipelinesCreate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pipelinesCreate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pipelinesCreate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pipelinesCreate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pipelinesList - errors', () => {
      it('should have a pipelinesList function', (done) => {
        try {
          assert.equal(true, typeof a.pipelinesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pipelinesList(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pipelinesList('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pipelinesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pipelinesGet - errors', () => {
      it('should have a pipelinesGet function', (done) => {
        try {
          assert.equal(true, typeof a.pipelinesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pipelinesGet(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pipelinesGet('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.pipelinesGet('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pipelinesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pipelinesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#previewPreview - errors', () => {
      it('should have a previewPreview function', (done) => {
        try {
          assert.equal(true, typeof a.previewPreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.previewPreview(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-previewPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.previewPreview('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-previewPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.previewPreview('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-previewPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.previewPreview('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-previewPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.previewPreview('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-previewPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runsList - errors', () => {
      it('should have a runsList function', (done) => {
        try {
          assert.equal(true, typeof a.runsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.runsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.runsList('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.runsList('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.runsList('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runsRunPipeline - errors', () => {
      it('should have a runsRunPipeline function', (done) => {
        try {
          assert.equal(true, typeof a.runsRunPipeline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.runsRunPipeline(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsRunPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.runsRunPipeline('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsRunPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.runsRunPipeline('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsRunPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.runsRunPipeline('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsRunPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.runsRunPipeline('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsRunPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runsGet - errors', () => {
      it('should have a runsGet function', (done) => {
        try {
          assert.equal(true, typeof a.runsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.runsGet(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.runsGet('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.runsGet('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runId', (done) => {
        try {
          a.runsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'runId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.runsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-runsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#artifactsGet - errors', () => {
      it('should have a artifactsGet function', (done) => {
        try {
          assert.equal(true, typeof a.artifactsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.artifactsGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-artifactsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.artifactsGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-artifactsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.artifactsGet('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-artifactsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runId', (done) => {
        try {
          a.artifactsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'runId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-artifactsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing artifactName', (done) => {
        try {
          a.artifactsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'artifactName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-artifactsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.artifactsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-artifactsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logsList - errors', () => {
      it('should have a logsList function', (done) => {
        try {
          assert.equal(true, typeof a.logsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.logsList(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.logsList('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.logsList('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runId', (done) => {
        try {
          a.logsList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'runId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.logsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logsGet - errors', () => {
      it('should have a logsGet function', (done) => {
        try {
          assert.equal(true, typeof a.logsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.logsGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.logsGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.logsGet('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runId', (done) => {
        try {
          a.logsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'runId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logId', (done) => {
        try {
          a.logsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'logId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.logsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-logsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesGetDeletedRepositories - errors', () => {
      it('should have a repositoriesGetDeletedRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.repositoriesGetDeletedRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.repositoriesGetDeletedRepositories(null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetDeletedRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.repositoriesGetDeletedRepositories('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetDeletedRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.repositoriesGetDeletedRepositories('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetDeletedRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesGetRecycleBinRepositories - errors', () => {
      it('should have a repositoriesGetRecycleBinRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.repositoriesGetRecycleBinRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.repositoriesGetRecycleBinRepositories(null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetRecycleBinRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.repositoriesGetRecycleBinRepositories('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetRecycleBinRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.repositoriesGetRecycleBinRepositories('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetRecycleBinRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesDeleteRepositoryFromRecycleBin - errors', () => {
      it('should have a repositoriesDeleteRepositoryFromRecycleBin function', (done) => {
        try {
          assert.equal(true, typeof a.repositoriesDeleteRepositoryFromRecycleBin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.repositoriesDeleteRepositoryFromRecycleBin(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesDeleteRepositoryFromRecycleBin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.repositoriesDeleteRepositoryFromRecycleBin('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesDeleteRepositoryFromRecycleBin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.repositoriesDeleteRepositoryFromRecycleBin('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesDeleteRepositoryFromRecycleBin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.repositoriesDeleteRepositoryFromRecycleBin('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesDeleteRepositoryFromRecycleBin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesRestoreRepositoryFromRecycleBin - errors', () => {
      it('should have a repositoriesRestoreRepositoryFromRecycleBin function', (done) => {
        try {
          assert.equal(true, typeof a.repositoriesRestoreRepositoryFromRecycleBin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.repositoriesRestoreRepositoryFromRecycleBin(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesRestoreRepositoryFromRecycleBin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.repositoriesRestoreRepositoryFromRecycleBin('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesRestoreRepositoryFromRecycleBin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.repositoriesRestoreRepositoryFromRecycleBin('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesRestoreRepositoryFromRecycleBin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.repositoriesRestoreRepositoryFromRecycleBin('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesRestoreRepositoryFromRecycleBin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.repositoriesRestoreRepositoryFromRecycleBin('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesRestoreRepositoryFromRecycleBin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesCreate - errors', () => {
      it('should have a repositoriesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.repositoriesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.repositoriesCreate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.repositoriesCreate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.repositoriesCreate('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.repositoriesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesList - errors', () => {
      it('should have a repositoriesList function', (done) => {
        try {
          assert.equal(true, typeof a.repositoriesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.repositoriesList(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.repositoriesList('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.repositoriesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesDelete - errors', () => {
      it('should have a repositoriesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.repositoriesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.repositoriesDelete(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.repositoriesDelete('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.repositoriesDelete('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.repositoriesDelete('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesGetRepository - errors', () => {
      it('should have a repositoriesGetRepository function', (done) => {
        try {
          assert.equal(true, typeof a.repositoriesGetRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.repositoriesGetRepository(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.repositoriesGetRepository('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.repositoriesGetRepository('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.repositoriesGetRepository('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesGetRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesUpdate - errors', () => {
      it('should have a repositoriesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.repositoriesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.repositoriesUpdate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.repositoriesUpdate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.repositoriesUpdate('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.repositoriesUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.repositoriesUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-repositoriesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsFavoritesCreate - errors', () => {
      it('should have a refsFavoritesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.refsFavoritesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.refsFavoritesCreate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.refsFavoritesCreate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.refsFavoritesCreate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.refsFavoritesCreate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsFavoritesList - errors', () => {
      it('should have a refsFavoritesList function', (done) => {
        try {
          assert.equal(true, typeof a.refsFavoritesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.refsFavoritesList(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.refsFavoritesList('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.refsFavoritesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsFavoritesDelete - errors', () => {
      it('should have a refsFavoritesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.refsFavoritesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.refsFavoritesDelete(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.refsFavoritesDelete('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing favoriteId', (done) => {
        try {
          a.refsFavoritesDelete('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'favoriteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.refsFavoritesDelete('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsFavoritesGet - errors', () => {
      it('should have a refsFavoritesGet function', (done) => {
        try {
          assert.equal(true, typeof a.refsFavoritesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.refsFavoritesGet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.refsFavoritesGet('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing favoriteId', (done) => {
        try {
          a.refsFavoritesGet('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'favoriteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.refsFavoritesGet('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsFavoritesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#policyConfigurationsGet - errors', () => {
      it('should have a policyConfigurationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.policyConfigurationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.policyConfigurationsGet(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-policyConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.policyConfigurationsGet('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-policyConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.policyConfigurationsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-policyConfigurationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsGetPullRequestsByProject - errors', () => {
      it('should have a pullRequestsGetPullRequestsByProject function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestsGetPullRequestsByProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestsGetPullRequestsByProject(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequestsByProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestsGetPullRequestsByProject('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequestsByProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestsGetPullRequestsByProject('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequestsByProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsGetPullRequestById - errors', () => {
      it('should have a pullRequestsGetPullRequestById function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestsGetPullRequestById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestsGetPullRequestById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequestById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestsGetPullRequestById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequestById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestsGetPullRequestById('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequestById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestsGetPullRequestById('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequestById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsCreate - errors', () => {
      it('should have a pullRequestsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestsCreate(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestsCreate('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestsCreate('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestsCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsGetPullRequests - errors', () => {
      it('should have a pullRequestsGetPullRequests function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestsGetPullRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestsGetPullRequests(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestsGetPullRequests('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestsGetPullRequests('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestsGetPullRequests('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsGetPullRequest - errors', () => {
      it('should have a pullRequestsGetPullRequest function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestsGetPullRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestsGetPullRequest(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestsGetPullRequest('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestsGetPullRequest('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestsGetPullRequest('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestsGetPullRequest('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsGetPullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsUpdate - errors', () => {
      it('should have a pullRequestsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestsUpdate(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestsUpdate('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestsUpdate('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestsUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#annotatedTagsCreate - errors', () => {
      it('should have a annotatedTagsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.annotatedTagsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.annotatedTagsCreate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.annotatedTagsCreate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.annotatedTagsCreate('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.annotatedTagsCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.annotatedTagsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#annotatedTagsGet - errors', () => {
      it('should have a annotatedTagsGet function', (done) => {
        try {
          assert.equal(true, typeof a.annotatedTagsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.annotatedTagsGet(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.annotatedTagsGet('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.annotatedTagsGet('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.annotatedTagsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.annotatedTagsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-annotatedTagsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#blobsGetBlobsZip - errors', () => {
      it('should have a blobsGetBlobsZip function', (done) => {
        try {
          assert.equal(true, typeof a.blobsGetBlobsZip === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.blobsGetBlobsZip(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlobsZip', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.blobsGetBlobsZip('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlobsZip', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.blobsGetBlobsZip('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlobsZip', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.blobsGetBlobsZip('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlobsZip', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.blobsGetBlobsZip('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlobsZip', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#blobsGetBlob - errors', () => {
      it('should have a blobsGetBlob function', (done) => {
        try {
          assert.equal(true, typeof a.blobsGetBlob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.blobsGetBlob(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.blobsGetBlob('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha1', (done) => {
        try {
          a.blobsGetBlob('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sha1 is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.blobsGetBlob('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.blobsGetBlob('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-blobsGetBlob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cherryPicksCreate - errors', () => {
      it('should have a cherryPicksCreate function', (done) => {
        try {
          assert.equal(true, typeof a.cherryPicksCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.cherryPicksCreate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.cherryPicksCreate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.cherryPicksCreate('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.cherryPicksCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.cherryPicksCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cherryPicksGetCherryPickForRefName - errors', () => {
      it('should have a cherryPicksGetCherryPickForRefName function', (done) => {
        try {
          assert.equal(true, typeof a.cherryPicksGetCherryPickForRefName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.cherryPicksGetCherryPickForRefName(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPickForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.cherryPicksGetCherryPickForRefName('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPickForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.cherryPicksGetCherryPickForRefName('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPickForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refName', (done) => {
        try {
          a.cherryPicksGetCherryPickForRefName('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'refName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPickForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.cherryPicksGetCherryPickForRefName('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPickForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cherryPicksGetCherryPick - errors', () => {
      it('should have a cherryPicksGetCherryPick function', (done) => {
        try {
          assert.equal(true, typeof a.cherryPicksGetCherryPick === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.cherryPicksGetCherryPick(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPick', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.cherryPicksGetCherryPick('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPick', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cherryPickId', (done) => {
        try {
          a.cherryPicksGetCherryPick('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'cherryPickId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPick', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.cherryPicksGetCherryPick('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPick', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.cherryPicksGetCherryPick('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-cherryPicksGetCherryPick', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitsGetPushCommits - errors', () => {
      it('should have a commitsGetPushCommits function', (done) => {
        try {
          assert.equal(true, typeof a.commitsGetPushCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.commitsGetPushCommits(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetPushCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.commitsGetPushCommits('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetPushCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pushId', (done) => {
        try {
          a.commitsGetPushCommits('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pushId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetPushCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.commitsGetPushCommits('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetPushCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.commitsGetPushCommits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetPushCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitsGet - errors', () => {
      it('should have a commitsGet function', (done) => {
        try {
          assert.equal(true, typeof a.commitsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.commitsGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.commitsGet('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.commitsGet('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.commitsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.commitsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitsGetChanges - errors', () => {
      it('should have a commitsGetChanges function', (done) => {
        try {
          assert.equal(true, typeof a.commitsGetChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.commitsGetChanges(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.commitsGetChanges('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.commitsGetChanges('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.commitsGetChanges('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.commitsGetChanges('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitsGetCommits - errors', () => {
      it('should have a commitsGetCommits function', (done) => {
        try {
          assert.equal(true, typeof a.commitsGetCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.commitsGetCommits(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.commitsGetCommits('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.commitsGetCommits('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitsGetCommitsBatch - errors', () => {
      it('should have a commitsGetCommitsBatch function', (done) => {
        try {
          assert.equal(true, typeof a.commitsGetCommitsBatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.commitsGetCommitsBatch(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetCommitsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.commitsGetCommitsBatch('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetCommitsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.commitsGetCommitsBatch('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetCommitsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.commitsGetCommitsBatch('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetCommitsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.commitsGetCommitsBatch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-commitsGetCommitsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statusesCreate - errors', () => {
      it('should have a statusesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.statusesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.statusesCreate(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.statusesCreate('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.statusesCreate('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.statusesCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.statusesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.statusesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statusesList - errors', () => {
      it('should have a statusesList function', (done) => {
        try {
          assert.equal(true, typeof a.statusesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.statusesList(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.statusesList('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.statusesList('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.statusesList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.statusesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diffsGet - errors', () => {
      it('should have a diffsGet function', (done) => {
        try {
          assert.equal(true, typeof a.diffsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.diffsGet(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-diffsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.diffsGet('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-diffsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.diffsGet('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-diffsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.diffsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-diffsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importRequestsCreate - errors', () => {
      it('should have a importRequestsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.importRequestsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.importRequestsCreate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importRequestsCreate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.importRequestsCreate('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.importRequestsCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.importRequestsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importRequestsQuery - errors', () => {
      it('should have a importRequestsQuery function', (done) => {
        try {
          assert.equal(true, typeof a.importRequestsQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.importRequestsQuery(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.importRequestsQuery('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.importRequestsQuery('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.importRequestsQuery('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importRequestsGet - errors', () => {
      it('should have a importRequestsGet function', (done) => {
        try {
          assert.equal(true, typeof a.importRequestsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.importRequestsGet(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.importRequestsGet('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.importRequestsGet('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing importRequestId', (done) => {
        try {
          a.importRequestsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'importRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.importRequestsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importRequestsUpdate - errors', () => {
      it('should have a importRequestsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.importRequestsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.importRequestsUpdate(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importRequestsUpdate('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.importRequestsUpdate('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.importRequestsUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing importRequestId', (done) => {
        try {
          a.importRequestsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'importRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.importRequestsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-importRequestsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#itemsList - errors', () => {
      it('should have a itemsList function', (done) => {
        try {
          assert.equal(true, typeof a.itemsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.itemsList(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-itemsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.itemsList('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-itemsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.itemsList('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-itemsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.itemsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-itemsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#itemsGetItemsBatch - errors', () => {
      it('should have a itemsGetItemsBatch function', (done) => {
        try {
          assert.equal(true, typeof a.itemsGetItemsBatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.itemsGetItemsBatch(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-itemsGetItemsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.itemsGetItemsBatch('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-itemsGetItemsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.itemsGetItemsBatch('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-itemsGetItemsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.itemsGetItemsBatch('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-itemsGetItemsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.itemsGetItemsBatch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-itemsGetItemsBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestQueryGet - errors', () => {
      it('should have a pullRequestQueryGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestQueryGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestQueryGet(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestQueryGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestQueryGet('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestQueryGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestQueryGet('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestQueryGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestQueryGet('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestQueryGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestQueryGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestQueryGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestAttachmentsList - errors', () => {
      it('should have a pullRequestAttachmentsList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestAttachmentsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestAttachmentsList(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestAttachmentsList('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestAttachmentsList('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestAttachmentsList('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestAttachmentsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestAttachmentsCreate - errors', () => {
      it('should have a pullRequestAttachmentsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestAttachmentsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestAttachmentsCreate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestAttachmentsCreate('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.pullRequestAttachmentsCreate('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestAttachmentsCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestAttachmentsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestAttachmentsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestAttachmentsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestAttachmentsDelete - errors', () => {
      it('should have a pullRequestAttachmentsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestAttachmentsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestAttachmentsDelete(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.pullRequestAttachmentsDelete('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestAttachmentsDelete('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestAttachmentsDelete('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestAttachmentsDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestAttachmentsDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestAttachmentsGet - errors', () => {
      it('should have a pullRequestAttachmentsGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestAttachmentsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestAttachmentsGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.pullRequestAttachmentsGet('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestAttachmentsGet('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestAttachmentsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestAttachmentsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestAttachmentsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestAttachmentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommitsGetPullRequestCommits - errors', () => {
      it('should have a pullRequestCommitsGetPullRequestCommits function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestCommitsGetPullRequestCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestCommits(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestCommits('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestCommits('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestCommits('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestCommits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommitsGetPullRequestIterationCommits - errors', () => {
      it('should have a pullRequestCommitsGetPullRequestIterationCommits function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestCommitsGetPullRequestIterationCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestIterationCommits(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestIterationCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestIterationCommits('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestIterationCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestIterationCommits('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestIterationCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iterationId', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestIterationCommits('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'iterationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestIterationCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestIterationCommits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestIterationCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestIterationCommits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommitsGetPullRequestIterationCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationsList - errors', () => {
      it('should have a pullRequestIterationsList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestIterationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestIterationsList(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestIterationsList('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestIterationsList('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestIterationsList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestIterationsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationsGet - errors', () => {
      it('should have a pullRequestIterationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestIterationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestIterationsGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestIterationsGet('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestIterationsGet('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iterationId', (done) => {
        try {
          a.pullRequestIterationsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'iterationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestIterationsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestIterationsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationChangesGet - errors', () => {
      it('should have a pullRequestIterationChangesGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestIterationChangesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestIterationChangesGet(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationChangesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestIterationChangesGet('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationChangesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestIterationChangesGet('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationChangesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iterationId', (done) => {
        try {
          a.pullRequestIterationChangesGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'iterationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationChangesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestIterationChangesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationChangesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestIterationChangesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationChangesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationStatusesCreate - errors', () => {
      it('should have a pullRequestIterationStatusesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestIterationStatusesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestIterationStatusesCreate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestIterationStatusesCreate('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestIterationStatusesCreate('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestIterationStatusesCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iterationId', (done) => {
        try {
          a.pullRequestIterationStatusesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'iterationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestIterationStatusesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestIterationStatusesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationStatusesList - errors', () => {
      it('should have a pullRequestIterationStatusesList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestIterationStatusesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestIterationStatusesList(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestIterationStatusesList('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestIterationStatusesList('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iterationId', (done) => {
        try {
          a.pullRequestIterationStatusesList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'iterationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestIterationStatusesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestIterationStatusesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationStatusesUpdate - errors', () => {
      it('should have a pullRequestIterationStatusesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestIterationStatusesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestIterationStatusesUpdate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestIterationStatusesUpdate('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestIterationStatusesUpdate('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestIterationStatusesUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iterationId', (done) => {
        try {
          a.pullRequestIterationStatusesUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'iterationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestIterationStatusesUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestIterationStatusesUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationStatusesDelete - errors', () => {
      it('should have a pullRequestIterationStatusesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestIterationStatusesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestIterationStatusesDelete(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestIterationStatusesDelete('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestIterationStatusesDelete('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iterationId', (done) => {
        try {
          a.pullRequestIterationStatusesDelete('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'iterationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing statusId', (done) => {
        try {
          a.pullRequestIterationStatusesDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'statusId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestIterationStatusesDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestIterationStatusesDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationStatusesGet - errors', () => {
      it('should have a pullRequestIterationStatusesGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestIterationStatusesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestIterationStatusesGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestIterationStatusesGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestIterationStatusesGet('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iterationId', (done) => {
        try {
          a.pullRequestIterationStatusesGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'iterationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing statusId', (done) => {
        try {
          a.pullRequestIterationStatusesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'statusId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestIterationStatusesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestIterationStatusesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestIterationStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestLabelsCreate - errors', () => {
      it('should have a pullRequestLabelsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestLabelsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestLabelsCreate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestLabelsCreate('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestLabelsCreate('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestLabelsCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestLabelsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestLabelsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestLabelsList - errors', () => {
      it('should have a pullRequestLabelsList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestLabelsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestLabelsList(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestLabelsList('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestLabelsList('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestLabelsList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestLabelsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestLabelsDelete - errors', () => {
      it('should have a pullRequestLabelsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestLabelsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestLabelsDelete(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestLabelsDelete('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestLabelsDelete('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelIdOrName', (done) => {
        try {
          a.pullRequestLabelsDelete('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'labelIdOrName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestLabelsDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestLabelsDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestLabelsGet - errors', () => {
      it('should have a pullRequestLabelsGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestLabelsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestLabelsGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestLabelsGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestLabelsGet('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelIdOrName', (done) => {
        try {
          a.pullRequestLabelsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'labelIdOrName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestLabelsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestLabelsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestLabelsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestPropertiesList - errors', () => {
      it('should have a pullRequestPropertiesList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestPropertiesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestPropertiesList(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestPropertiesList('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestPropertiesList('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestPropertiesList('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestPropertiesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestPropertiesUpdate - errors', () => {
      it('should have a pullRequestPropertiesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestPropertiesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestPropertiesUpdate(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestPropertiesUpdate('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestPropertiesUpdate('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestPropertiesUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestPropertiesUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestPropertiesUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestPropertiesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersCreatePullRequestReviewers - errors', () => {
      it('should have a pullRequestReviewersCreatePullRequestReviewers function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestReviewersCreatePullRequestReviewers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewers('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewers('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewers('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewers('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewers('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersCreateUnmaterializedPullRequestReviewer - errors', () => {
      it('should have a pullRequestReviewersCreateUnmaterializedPullRequestReviewer function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestReviewersCreateUnmaterializedPullRequestReviewer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestReviewersCreateUnmaterializedPullRequestReviewer(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreateUnmaterializedPullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestReviewersCreateUnmaterializedPullRequestReviewer('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreateUnmaterializedPullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestReviewersCreateUnmaterializedPullRequestReviewer('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreateUnmaterializedPullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestReviewersCreateUnmaterializedPullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreateUnmaterializedPullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestReviewersCreateUnmaterializedPullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreateUnmaterializedPullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestReviewersCreateUnmaterializedPullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreateUnmaterializedPullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersList - errors', () => {
      it('should have a pullRequestReviewersList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestReviewersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestReviewersList(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestReviewersList('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestReviewersList('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestReviewersList('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestReviewersList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersUpdatePullRequestReviewers - errors', () => {
      it('should have a pullRequestReviewersUpdatePullRequestReviewers function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestReviewersUpdatePullRequestReviewers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewers('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewers('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewers('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewers('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewers('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersCreatePullRequestReviewer - errors', () => {
      it('should have a pullRequestReviewersCreatePullRequestReviewer function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestReviewersCreatePullRequestReviewer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewer(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewer('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewer('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reviewerId', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'reviewerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersCreatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersDelete - errors', () => {
      it('should have a pullRequestReviewersDelete function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestReviewersDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestReviewersDelete(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestReviewersDelete('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestReviewersDelete('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reviewerId', (done) => {
        try {
          a.pullRequestReviewersDelete('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'reviewerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestReviewersDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestReviewersDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersGet - errors', () => {
      it('should have a pullRequestReviewersGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestReviewersGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestReviewersGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestReviewersGet('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestReviewersGet('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reviewerId', (done) => {
        try {
          a.pullRequestReviewersGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'reviewerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestReviewersGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestReviewersGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersUpdatePullRequestReviewer - errors', () => {
      it('should have a pullRequestReviewersUpdatePullRequestReviewer function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestReviewersUpdatePullRequestReviewer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewer(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewer('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewer('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reviewerId', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'reviewerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestReviewersUpdatePullRequestReviewer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestShareSharePullRequest - errors', () => {
      it('should have a pullRequestShareSharePullRequest function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestShareSharePullRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestShareSharePullRequest(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestShareSharePullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestShareSharePullRequest('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestShareSharePullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestShareSharePullRequest('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestShareSharePullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestShareSharePullRequest('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestShareSharePullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestShareSharePullRequest('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestShareSharePullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestShareSharePullRequest('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestShareSharePullRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestStatusesCreate - errors', () => {
      it('should have a pullRequestStatusesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestStatusesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestStatusesCreate(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestStatusesCreate('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestStatusesCreate('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestStatusesCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestStatusesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestStatusesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestStatusesList - errors', () => {
      it('should have a pullRequestStatusesList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestStatusesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestStatusesList(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestStatusesList('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestStatusesList('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestStatusesList('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestStatusesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestStatusesUpdate - errors', () => {
      it('should have a pullRequestStatusesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestStatusesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestStatusesUpdate(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestStatusesUpdate('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestStatusesUpdate('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestStatusesUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestStatusesUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestStatusesUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestStatusesDelete - errors', () => {
      it('should have a pullRequestStatusesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestStatusesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestStatusesDelete(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestStatusesDelete('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestStatusesDelete('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing statusId', (done) => {
        try {
          a.pullRequestStatusesDelete('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'statusId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestStatusesDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestStatusesDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestStatusesGet - errors', () => {
      it('should have a pullRequestStatusesGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestStatusesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestStatusesGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestStatusesGet('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestStatusesGet('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing statusId', (done) => {
        try {
          a.pullRequestStatusesGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'statusId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestStatusesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestStatusesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestStatusesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadsCreate - errors', () => {
      it('should have a pullRequestThreadsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestThreadsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestThreadsCreate(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestThreadsCreate('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestThreadsCreate('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestThreadsCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestThreadsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestThreadsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadsList - errors', () => {
      it('should have a pullRequestThreadsList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestThreadsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestThreadsList(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestThreadsList('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestThreadsList('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestThreadsList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestThreadsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadsGet - errors', () => {
      it('should have a pullRequestThreadsGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestThreadsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestThreadsGet(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestThreadsGet('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestThreadsGet('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestThreadsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestThreadsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestThreadsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadsUpdate - errors', () => {
      it('should have a pullRequestThreadsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestThreadsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestThreadsUpdate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestThreadsUpdate('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestThreadsUpdate('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestThreadsUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestThreadsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestThreadsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestThreadsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadCommentsCreate - errors', () => {
      it('should have a pullRequestThreadCommentsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestThreadCommentsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestThreadCommentsCreate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestThreadCommentsCreate('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestThreadCommentsCreate('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestThreadCommentsCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestThreadCommentsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestThreadCommentsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestThreadCommentsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadCommentsList - errors', () => {
      it('should have a pullRequestThreadCommentsList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestThreadCommentsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestThreadCommentsList(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestThreadCommentsList('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestThreadCommentsList('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestThreadCommentsList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestThreadCommentsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestThreadCommentsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadCommentsDelete - errors', () => {
      it('should have a pullRequestThreadCommentsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestThreadCommentsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestThreadCommentsDelete(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestThreadCommentsDelete('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestThreadCommentsDelete('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestThreadCommentsDelete('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.pullRequestThreadCommentsDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestThreadCommentsDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestThreadCommentsDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadCommentsGet - errors', () => {
      it('should have a pullRequestThreadCommentsGet function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestThreadCommentsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestThreadCommentsGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestThreadCommentsGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestThreadCommentsGet('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestThreadCommentsGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.pullRequestThreadCommentsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestThreadCommentsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestThreadCommentsGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadCommentsUpdate - errors', () => {
      it('should have a pullRequestThreadCommentsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestThreadCommentsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestThreadCommentsUpdate(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pullRequestThreadCommentsUpdate('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestThreadCommentsUpdate('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestThreadCommentsUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestThreadCommentsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.pullRequestThreadCommentsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestThreadCommentsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestThreadCommentsUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestThreadCommentsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommentLikesCreate - errors', () => {
      it('should have a pullRequestCommentLikesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestCommentLikesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestCommentLikesCreate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestCommentLikesCreate('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestCommentLikesCreate('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestCommentLikesCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.pullRequestCommentLikesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestCommentLikesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestCommentLikesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommentLikesDelete - errors', () => {
      it('should have a pullRequestCommentLikesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestCommentLikesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestCommentLikesDelete(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestCommentLikesDelete('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestCommentLikesDelete('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestCommentLikesDelete('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.pullRequestCommentLikesDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestCommentLikesDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestCommentLikesDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommentLikesList - errors', () => {
      it('should have a pullRequestCommentLikesList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestCommentLikesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestCommentLikesList(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestCommentLikesList('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestCommentLikesList('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threadId', (done) => {
        try {
          a.pullRequestCommentLikesList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'threadId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.pullRequestCommentLikesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestCommentLikesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestCommentLikesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestCommentLikesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestWorkItemsList - errors', () => {
      it('should have a pullRequestWorkItemsList function', (done) => {
        try {
          assert.equal(true, typeof a.pullRequestWorkItemsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pullRequestWorkItemsList(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestWorkItemsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pullRequestWorkItemsList('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestWorkItemsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.pullRequestWorkItemsList('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestWorkItemsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pullRequestWorkItemsList('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestWorkItemsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pullRequestWorkItemsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pullRequestWorkItemsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushesCreate - errors', () => {
      it('should have a pushesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.pushesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pushesCreate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pushesCreate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pushesCreate('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pushesCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pushesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushesList - errors', () => {
      it('should have a pushesList function', (done) => {
        try {
          assert.equal(true, typeof a.pushesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pushesList(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pushesList('fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pushesList('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pushesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushesGet - errors', () => {
      it('should have a pushesGet function', (done) => {
        try {
          assert.equal(true, typeof a.pushesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.pushesGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.pushesGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pushId', (done) => {
        try {
          a.pushesGet('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pushId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.pushesGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.pushesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-pushesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsList - errors', () => {
      it('should have a refsList function', (done) => {
        try {
          assert.equal(true, typeof a.refsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.refsList(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.refsList('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.refsList('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.refsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsUpdateRef - errors', () => {
      it('should have a refsUpdateRef function', (done) => {
        try {
          assert.equal(true, typeof a.refsUpdateRef === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.refsUpdateRef(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.refsUpdateRef('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.refsUpdateRef('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.refsUpdateRef('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.refsUpdateRef('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.refsUpdateRef('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsUpdateRefs - errors', () => {
      it('should have a refsUpdateRefs function', (done) => {
        try {
          assert.equal(true, typeof a.refsUpdateRefs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.refsUpdateRefs(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRefs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.refsUpdateRefs('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRefs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.refsUpdateRefs('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRefs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.refsUpdateRefs('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRefs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.refsUpdateRefs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-refsUpdateRefs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revertsCreate - errors', () => {
      it('should have a revertsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.revertsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.revertsCreate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.revertsCreate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.revertsCreate('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.revertsCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.revertsCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revertsGetRevertForRefName - errors', () => {
      it('should have a revertsGetRevertForRefName function', (done) => {
        try {
          assert.equal(true, typeof a.revertsGetRevertForRefName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.revertsGetRevertForRefName(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevertForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.revertsGetRevertForRefName('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevertForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.revertsGetRevertForRefName('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevertForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refName', (done) => {
        try {
          a.revertsGetRevertForRefName('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'refName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevertForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.revertsGetRevertForRefName('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevertForRefName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revertsGetRevert - errors', () => {
      it('should have a revertsGetRevert function', (done) => {
        try {
          assert.equal(true, typeof a.revertsGetRevert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.revertsGetRevert(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.revertsGetRevert('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing revertId', (done) => {
        try {
          a.revertsGetRevert('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'revertId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.revertsGetRevert('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.revertsGetRevert('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-revertsGetRevert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsList - errors', () => {
      it('should have a statsList function', (done) => {
        try {
          assert.equal(true, typeof a.statsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.statsList(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.statsList('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.statsList('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.statsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-statsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#suggestionsList - errors', () => {
      it('should have a suggestionsList function', (done) => {
        try {
          assert.equal(true, typeof a.suggestionsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.suggestionsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-suggestionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.suggestionsList('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-suggestionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.suggestionsList('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-suggestionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.suggestionsList('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-suggestionsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#treesGet - errors', () => {
      it('should have a treesGet function', (done) => {
        try {
          assert.equal(true, typeof a.treesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.treesGet(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-treesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.treesGet('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-treesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha1', (done) => {
        try {
          a.treesGet('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sha1 is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-treesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.treesGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-treesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.treesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-treesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mergeBasesList - errors', () => {
      it('should have a mergeBasesList function', (done) => {
        try {
          assert.equal(true, typeof a.mergeBasesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.mergeBasesList(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergeBasesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryNameOrId', (done) => {
        try {
          a.mergeBasesList('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryNameOrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergeBasesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.mergeBasesList('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergeBasesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing otherCommitId', (done) => {
        try {
          a.mergeBasesList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'otherCommitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergeBasesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.mergeBasesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergeBasesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.mergeBasesList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergeBasesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forksList - errors', () => {
      it('should have a forksList function', (done) => {
        try {
          assert.equal(true, typeof a.forksList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.forksList(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryNameOrId', (done) => {
        try {
          a.forksList('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryNameOrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionId', (done) => {
        try {
          a.forksList('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'collectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.forksList('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.forksList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forksCreateForkSyncRequest - errors', () => {
      it('should have a forksCreateForkSyncRequest function', (done) => {
        try {
          assert.equal(true, typeof a.forksCreateForkSyncRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.forksCreateForkSyncRequest(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksCreateForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.forksCreateForkSyncRequest('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksCreateForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryNameOrId', (done) => {
        try {
          a.forksCreateForkSyncRequest('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryNameOrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksCreateForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.forksCreateForkSyncRequest('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksCreateForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.forksCreateForkSyncRequest('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksCreateForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forksGetForkSyncRequests - errors', () => {
      it('should have a forksGetForkSyncRequests function', (done) => {
        try {
          assert.equal(true, typeof a.forksGetForkSyncRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.forksGetForkSyncRequests(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksGetForkSyncRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryNameOrId', (done) => {
        try {
          a.forksGetForkSyncRequests('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryNameOrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksGetForkSyncRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.forksGetForkSyncRequests('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksGetForkSyncRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.forksGetForkSyncRequests('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksGetForkSyncRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forksGetForkSyncRequest - errors', () => {
      it('should have a forksGetForkSyncRequest function', (done) => {
        try {
          assert.equal(true, typeof a.forksGetForkSyncRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.forksGetForkSyncRequest(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksGetForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryNameOrId', (done) => {
        try {
          a.forksGetForkSyncRequest('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryNameOrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksGetForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing forkSyncOperationId', (done) => {
        try {
          a.forksGetForkSyncRequest('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'forkSyncOperationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksGetForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.forksGetForkSyncRequest('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksGetForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.forksGetForkSyncRequest('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-forksGetForkSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mergesCreate - errors', () => {
      it('should have a mergesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.mergesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.mergesCreate(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.mergesCreate('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.mergesCreate('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryNameOrId', (done) => {
        try {
          a.mergesCreate('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryNameOrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.mergesCreate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mergesGet - errors', () => {
      it('should have a mergesGet function', (done) => {
        try {
          assert.equal(true, typeof a.mergesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.mergesGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.mergesGet('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryNameOrId', (done) => {
        try {
          a.mergesGet('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositoryNameOrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeOperationId', (done) => {
        try {
          a.mergesGet('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'mergeOperationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiVersion', (done) => {
        try {
          a.mergesGet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apiVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-azure_devops-adapter-mergesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
