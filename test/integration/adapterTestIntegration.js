/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-azure_devops',
      type: 'AzureDevops',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const AzureDevops = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Azure_devops Adapter Test', () => {
  describe('AzureDevops Class Tests', () => {
    const a = new AzureDevops(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const pipelinesOrganization = 'fakedata';
    const pipelinesProject = 'fakedata';
    const pipelinesApiVersion = 'fakedata';
    let pipelinesPipelineId = 555;
    const pipelinesPipelinesCreateBodyParam = {
      configuration: null,
      folder: 'string',
      name: 'string'
    };
    describe('#pipelinesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pipelinesCreate(pipelinesOrganization, pipelinesPipelinesCreateBodyParam, pipelinesProject, pipelinesApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.folder);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(7, data.response.revision);
              } else {
                runCommonAsserts(data, error);
              }
              pipelinesPipelineId = data.response.id;
              saveMockData('Pipelines', 'pipelinesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pipelinesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pipelinesList(pipelinesOrganization, pipelinesProject, null, null, null, pipelinesApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'pipelinesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pipelinesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pipelinesGet(pipelinesOrganization, pipelinesProject, pipelinesPipelineId, null, pipelinesApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.folder);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(6, data.response.revision);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'pipelinesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const previewOrganization = 'fakedata';
    const previewProject = 'fakedata';
    const previewPipelineId = 555;
    const previewApiVersion = 'fakedata';
    const previewPreviewPreviewBodyParam = {
      previewRun: false,
      resources: null,
      stagesToSkip: [
        'string'
      ],
      templateParameters: {},
      variables: {},
      yamlOverride: 'string'
    };
    describe('#previewPreview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.previewPreview(previewOrganization, previewPreviewPreviewBodyParam, previewProject, previewPipelineId, null, previewApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.finalYaml);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Preview', 'previewPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runsOrganization = 'fakedata';
    const runsProject = 'fakedata';
    let runsPipelineId = 555;
    const runsApiVersion = 'fakedata';
    let runsRunId = 555;
    const runsRunsRunPipelineBodyParam = {
      previewRun: false,
      resources: null,
      stagesToSkip: [
        'string'
      ],
      templateParameters: {},
      variables: {},
      yamlOverride: 'string'
    };
    describe('#runsRunPipeline - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runsRunPipeline(runsOrganization, runsRunsRunPipelineBodyParam, runsProject, runsPipelineId, null, runsApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              runsPipelineId = data.response.id;
              runsRunId = data.response.id;
              saveMockData('Runs', 'runsRunPipeline', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runsList(runsOrganization, runsProject, runsPipelineId, runsApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Runs', 'runsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runsGet(runsOrganization, runsProject, runsPipelineId, runsRunId, runsApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Runs', 'runsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const artifactsOrganization = 'fakedata';
    const artifactsProject = 'fakedata';
    const artifactsPipelineId = 555;
    const artifactsRunId = 555;
    const artifactsArtifactName = 'fakedata';
    const artifactsApiVersion = 'fakedata';
    describe('#artifactsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.artifactsGet(artifactsOrganization, artifactsProject, artifactsPipelineId, artifactsRunId, artifactsArtifactName, null, artifactsApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Artifacts', 'artifactsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const logsOrganization = 'fakedata';
    const logsProject = 'fakedata';
    const logsPipelineId = 555;
    const logsRunId = 555;
    const logsApiVersion = 'fakedata';
    describe('#logsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.logsList(logsOrganization, logsProject, logsPipelineId, logsRunId, null, logsApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.logs));
                assert.equal('object', typeof data.response.signedContent);
                assert.equal('string', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Logs', 'logsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const logsLogId = 555;
    describe('#logsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.logsGet(logsOrganization, logsProject, logsPipelineId, logsRunId, logsLogId, null, logsApiVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.createdOn);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.lastChangedOn);
                assert.equal(5, data.response.lineCount);
                assert.equal('object', typeof data.response.signedContent);
                assert.equal('string', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Logs', 'logsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesGetDeletedRepositories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.repositoriesGetDeletedRepositories('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'repositoriesGetDeletedRepositories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesGetRecycleBinRepositories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.repositoriesGetRecycleBinRepositories('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'repositoriesGetRecycleBinRepositories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesDeleteRepositoryFromRecycleBin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.repositoriesDeleteRepositoryFromRecycleBin('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'repositoriesDeleteRepositoryFromRecycleBin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repositoriesRepositoriesRestoreRepositoryFromRecycleBinBodyParam = {
      deleted: false
    };
    describe('#repositoriesRestoreRepositoryFromRecycleBin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.repositoriesRestoreRepositoryFromRecycleBin('fakedata', repositoriesRepositoriesRestoreRepositoryFromRecycleBinBodyParam, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'repositoriesRestoreRepositoryFromRecycleBin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repositoriesRepositoriesCreateBodyParam = {
      name: 'string',
      parentRepository: {
        collection: {
          id: 'string',
          name: 'string',
          url: 'string'
        },
        id: 'string',
        isFork: false,
        name: 'string',
        project: {
          abbreviation: 'string',
          defaultTeamImageUrl: 'string',
          description: 'string',
          id: 'string',
          lastUpdateTime: 'string',
          name: 'string',
          revision: 4,
          state: 'deleting',
          url: 'string',
          visibility: 'private'
        },
        remoteUrl: 'string',
        sshUrl: 'string',
        url: 'string'
      },
      project: {
        abbreviation: 'string',
        defaultTeamImageUrl: 'string',
        description: 'string',
        id: 'string',
        lastUpdateTime: 'string',
        name: 'string',
        revision: 3,
        state: 'wellFormed',
        url: 'string',
        visibility: 'private'
      }
    };
    describe('#repositoriesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.repositoriesCreate('fakedata', repositoriesRepositoriesCreateBodyParam, 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'repositoriesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.repositoriesList('fakedata', 'fakedata', null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'repositoriesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.repositoriesDelete('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'repositoriesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#repositoriesGetRepository - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.repositoriesGetRepository('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'repositoriesGetRepository', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repositoriesRepositoriesUpdateBodyParam = {
      _links: {
        links: {}
      },
      defaultBranch: 'string',
      id: 'string',
      isDisabled: true,
      isFork: false,
      name: 'string',
      parentRepository: {
        collection: {
          id: 'string',
          name: 'string',
          url: 'string'
        },
        id: 'string',
        isFork: true,
        name: 'string',
        project: {
          abbreviation: 'string',
          defaultTeamImageUrl: 'string',
          description: 'string',
          id: 'string',
          lastUpdateTime: 'string',
          name: 'string',
          revision: 4,
          state: 'all',
          url: 'string',
          visibility: 'public'
        },
        remoteUrl: 'string',
        sshUrl: 'string',
        url: 'string'
      },
      project: {
        abbreviation: 'string',
        defaultTeamImageUrl: 'string',
        description: 'string',
        id: 'string',
        lastUpdateTime: 'string',
        name: 'string',
        revision: 2,
        state: 'deleted',
        url: 'string',
        visibility: 'private'
      },
      remoteUrl: 'string',
      size: 6,
      sshUrl: 'string',
      url: 'string',
      validRemoteUrls: [
        'string'
      ],
      webUrl: 'string'
    };
    describe('#repositoriesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.repositoriesUpdate('fakedata', repositoriesRepositoriesUpdateBodyParam, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'repositoriesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const refsFavoritesRefsFavoritesCreateBodyParam = {
      _links: {
        links: {}
      },
      id: 2,
      identityId: 'string',
      name: 'string',
      repositoryId: 'string',
      type: 'invalid',
      url: 'string'
    };
    describe('#refsFavoritesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refsFavoritesCreate('fakedata', refsFavoritesRefsFavoritesCreateBodyParam, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RefsFavorites', 'refsFavoritesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsFavoritesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.refsFavoritesList('fakedata', 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RefsFavorites', 'refsFavoritesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsFavoritesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refsFavoritesDelete('fakedata', 'fakedata', 555, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RefsFavorites', 'refsFavoritesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsFavoritesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refsFavoritesGet('fakedata', 'fakedata', 555, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RefsFavorites', 'refsFavoritesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#policyConfigurationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.policyConfigurationsGet('fakedata', 'fakedata', null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyConfigurations', 'policyConfigurationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsGetPullRequestsByProject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestsGetPullRequestsByProject('fakedata', 'fakedata', null, null, null, null, null, null, null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequests', 'pullRequestsGetPullRequestsByProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsGetPullRequestById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestsGetPullRequestById('fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequests', 'pullRequestsGetPullRequestById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestsPullRequestsCreateBodyParam = {
      _links: {
        links: {}
      },
      artifactId: 'string',
      autoCompleteSetBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      closedBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      closedDate: 'string',
      codeReviewId: 1,
      commits: [
        {
          _links: {
            links: {}
          },
          author: {
            date: 'string',
            email: 'string',
            imageUrl: 'string',
            name: 'string'
          },
          changeCounts: {},
          changes: [
            {
              changeType: 'property',
              item: 'string',
              newContent: {
                content: 'string',
                contentType: 'base64Encoded'
              },
              sourceServerItem: 'string',
              url: 'string'
            }
          ],
          comment: 'string',
          commentTruncated: true,
          commitId: 'string',
          committer: {
            date: 'string',
            email: 'string',
            imageUrl: 'string',
            name: 'string'
          },
          parents: [
            'string'
          ],
          push: {
            _links: {
              links: {}
            },
            date: 'string',
            pushedBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            pushId: 1,
            url: 'string'
          },
          remoteUrl: 'string',
          statuses: [
            {
              _links: {
                links: {}
              },
              context: {
                genre: 'string',
                name: 'string'
              },
              createdBy: {
                _links: {
                  links: {}
                },
                descriptor: 'string',
                displayName: 'string',
                url: 'string'
              },
              creationDate: 'string',
              description: 'string',
              id: 1,
              state: 'succeeded',
              targetUrl: 'string',
              updatedDate: 'string'
            }
          ],
          url: 'string',
          workItems: [
            {
              id: 'string',
              url: 'string'
            }
          ]
        }
      ],
      completionOptions: {
        autoCompleteIgnoreConfigIds: [
          3
        ],
        bypassPolicy: false,
        bypassReason: 'string',
        deleteSourceBranch: false,
        mergeCommitMessage: 'string',
        mergeStrategy: 'rebaseMerge',
        squashMerge: false,
        transitionWorkItems: true,
        triggeredByAutoComplete: true
      },
      completionQueueTime: 'string',
      createdBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      creationDate: 'string',
      description: 'string',
      forkSource: {
        _links: {
          links: {}
        },
        creator: {
          _links: {
            links: {}
          },
          descriptor: 'string',
          displayName: 'string',
          url: 'string'
        },
        isLocked: false,
        isLockedBy: {
          _links: {
            links: {}
          },
          descriptor: 'string',
          displayName: 'string',
          url: 'string'
        },
        name: 'string',
        objectId: 'string',
        peeledObjectId: 'string',
        statuses: [
          {
            _links: {
              links: {}
            },
            context: {
              genre: 'string',
              name: 'string'
            },
            createdBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            creationDate: 'string',
            description: 'string',
            id: 1,
            state: 'notSet',
            targetUrl: 'string',
            updatedDate: 'string'
          }
        ],
        url: 'string'
      },
      hasMultipleMergeBases: false,
      isDraft: false,
      labels: [
        {
          active: false,
          id: 'string',
          name: 'string',
          url: 'string'
        }
      ],
      lastMergeCommit: {
        _links: {
          links: {}
        },
        author: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        changeCounts: {},
        changes: [
          {
            changeType: 'rollback',
            item: 'string',
            newContent: {
              content: 'string',
              contentType: 'rawText'
            },
            sourceServerItem: 'string',
            url: 'string'
          }
        ],
        comment: 'string',
        commentTruncated: false,
        commitId: 'string',
        committer: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        parents: [
          'string'
        ],
        push: {
          _links: {
            links: {}
          },
          date: 'string',
          pushedBy: {
            _links: {
              links: {}
            },
            descriptor: 'string',
            displayName: 'string',
            url: 'string'
          },
          pushId: 3,
          url: 'string'
        },
        remoteUrl: 'string',
        statuses: [
          {
            _links: {
              links: {}
            },
            context: {
              genre: 'string',
              name: 'string'
            },
            createdBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            creationDate: 'string',
            description: 'string',
            id: 1,
            state: 'notApplicable',
            targetUrl: 'string',
            updatedDate: 'string'
          }
        ],
        url: 'string',
        workItems: [
          {
            id: 'string',
            url: 'string'
          }
        ]
      },
      lastMergeSourceCommit: {
        _links: {
          links: {}
        },
        author: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        changeCounts: {},
        changes: [
          {
            changeType: 'rename',
            item: 'string',
            newContent: {
              content: 'string',
              contentType: 'rawText'
            },
            sourceServerItem: 'string',
            url: 'string'
          }
        ],
        comment: 'string',
        commentTruncated: false,
        commitId: 'string',
        committer: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        parents: [
          'string'
        ],
        push: {
          _links: {
            links: {}
          },
          date: 'string',
          pushedBy: {
            _links: {
              links: {}
            },
            descriptor: 'string',
            displayName: 'string',
            url: 'string'
          },
          pushId: 3,
          url: 'string'
        },
        remoteUrl: 'string',
        statuses: [
          {
            _links: {
              links: {}
            },
            context: {
              genre: 'string',
              name: 'string'
            },
            createdBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            creationDate: 'string',
            description: 'string',
            id: 1,
            state: 'failed',
            targetUrl: 'string',
            updatedDate: 'string'
          }
        ],
        url: 'string',
        workItems: [
          {
            id: 'string',
            url: 'string'
          }
        ]
      },
      lastMergeTargetCommit: {
        _links: {
          links: {}
        },
        author: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        changeCounts: {},
        changes: [
          {
            changeType: 'branch',
            item: 'string',
            newContent: {
              content: 'string',
              contentType: 'base64Encoded'
            },
            sourceServerItem: 'string',
            url: 'string'
          }
        ],
        comment: 'string',
        commentTruncated: true,
        commitId: 'string',
        committer: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        parents: [
          'string'
        ],
        push: {
          _links: {
            links: {}
          },
          date: 'string',
          pushedBy: {
            _links: {
              links: {}
            },
            descriptor: 'string',
            displayName: 'string',
            url: 'string'
          },
          pushId: 10,
          url: 'string'
        },
        remoteUrl: 'string',
        statuses: [
          {
            _links: {
              links: {}
            },
            context: {
              genre: 'string',
              name: 'string'
            },
            createdBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            creationDate: 'string',
            description: 'string',
            id: 8,
            state: 'succeeded',
            targetUrl: 'string',
            updatedDate: 'string'
          }
        ],
        url: 'string',
        workItems: [
          {
            id: 'string',
            url: 'string'
          }
        ]
      },
      mergeFailureMessage: 'string',
      mergeFailureType: 'caseSensitive',
      mergeId: 'string',
      mergeOptions: {
        conflictAuthorshipCommits: false,
        detectRenameFalsePositives: false,
        disableRenames: true
      },
      mergeStatus: 'notSet',
      pullRequestId: 10,
      remoteUrl: 'string',
      repository: {
        _links: {
          links: {}
        },
        defaultBranch: 'string',
        id: 'string',
        isDisabled: false,
        isFork: true,
        name: 'string',
        parentRepository: {
          collection: {
            id: 'string',
            name: 'string',
            url: 'string'
          },
          id: 'string',
          isFork: false,
          name: 'string',
          project: {
            abbreviation: 'string',
            defaultTeamImageUrl: 'string',
            description: 'string',
            id: 'string',
            lastUpdateTime: 'string',
            name: 'string',
            revision: 7,
            state: 'deleted',
            url: 'string',
            visibility: 'private'
          },
          remoteUrl: 'string',
          sshUrl: 'string',
          url: 'string'
        },
        project: {
          abbreviation: 'string',
          defaultTeamImageUrl: 'string',
          description: 'string',
          id: 'string',
          lastUpdateTime: 'string',
          name: 'string',
          revision: 10,
          state: 'deleted',
          url: 'string',
          visibility: 'private'
        },
        remoteUrl: 'string',
        size: 5,
        sshUrl: 'string',
        url: 'string',
        validRemoteUrls: [
          'string'
        ],
        webUrl: 'string'
      },
      reviewers: [
        {
          _links: {
            links: {}
          },
          descriptor: 'string',
          displayName: 'string',
          url: 'string'
        }
      ],
      sourceRefName: 'string',
      status: 'active',
      supportsIterations: false,
      targetRefName: 'string',
      title: 'string',
      url: 'string',
      workItemRefs: [
        {
          id: 'string',
          url: 'string'
        }
      ]
    };
    describe('#pullRequestsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestsCreate('fakedata', pullRequestsPullRequestsCreateBodyParam, 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequests', 'pullRequestsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsGetPullRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestsGetPullRequests('fakedata', 'fakedata', 'fakedata', null, null, null, null, null, null, null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequests', 'pullRequestsGetPullRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestsGetPullRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestsGetPullRequest('fakedata', 'fakedata', 555, 'fakedata', null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequests', 'pullRequestsGetPullRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestsPullRequestsUpdateBodyParam = {
      _links: {
        links: {}
      },
      artifactId: 'string',
      autoCompleteSetBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      closedBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      closedDate: 'string',
      codeReviewId: 4,
      commits: [
        {
          _links: {
            links: {}
          },
          author: {
            date: 'string',
            email: 'string',
            imageUrl: 'string',
            name: 'string'
          },
          changeCounts: {},
          changes: [
            {
              changeType: 'encoding',
              item: 'string',
              newContent: {
                content: 'string',
                contentType: 'rawText'
              },
              sourceServerItem: 'string',
              url: 'string'
            }
          ],
          comment: 'string',
          commentTruncated: true,
          commitId: 'string',
          committer: {
            date: 'string',
            email: 'string',
            imageUrl: 'string',
            name: 'string'
          },
          parents: [
            'string'
          ],
          push: {
            _links: {
              links: {}
            },
            date: 'string',
            pushedBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            pushId: 6,
            url: 'string'
          },
          remoteUrl: 'string',
          statuses: [
            {
              _links: {
                links: {}
              },
              context: {
                genre: 'string',
                name: 'string'
              },
              createdBy: {
                _links: {
                  links: {}
                },
                descriptor: 'string',
                displayName: 'string',
                url: 'string'
              },
              creationDate: 'string',
              description: 'string',
              id: 9,
              state: 'succeeded',
              targetUrl: 'string',
              updatedDate: 'string'
            }
          ],
          url: 'string',
          workItems: [
            {
              id: 'string',
              url: 'string'
            }
          ]
        }
      ],
      completionOptions: {
        autoCompleteIgnoreConfigIds: [
          1
        ],
        bypassPolicy: false,
        bypassReason: 'string',
        deleteSourceBranch: false,
        mergeCommitMessage: 'string',
        mergeStrategy: 'noFastForward',
        squashMerge: true,
        transitionWorkItems: true,
        triggeredByAutoComplete: false
      },
      completionQueueTime: 'string',
      createdBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      creationDate: 'string',
      description: 'string',
      forkSource: {
        _links: {
          links: {}
        },
        creator: {
          _links: {
            links: {}
          },
          descriptor: 'string',
          displayName: 'string',
          url: 'string'
        },
        isLocked: true,
        isLockedBy: {
          _links: {
            links: {}
          },
          descriptor: 'string',
          displayName: 'string',
          url: 'string'
        },
        name: 'string',
        objectId: 'string',
        peeledObjectId: 'string',
        statuses: [
          {
            _links: {
              links: {}
            },
            context: {
              genre: 'string',
              name: 'string'
            },
            createdBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            creationDate: 'string',
            description: 'string',
            id: 8,
            state: 'error',
            targetUrl: 'string',
            updatedDate: 'string'
          }
        ],
        url: 'string'
      },
      hasMultipleMergeBases: false,
      isDraft: true,
      labels: [
        {
          active: false,
          id: 'string',
          name: 'string',
          url: 'string'
        }
      ],
      lastMergeCommit: {
        _links: {
          links: {}
        },
        author: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        changeCounts: {},
        changes: [
          {
            changeType: 'branch',
            item: 'string',
            newContent: {
              content: 'string',
              contentType: 'base64Encoded'
            },
            sourceServerItem: 'string',
            url: 'string'
          }
        ],
        comment: 'string',
        commentTruncated: false,
        commitId: 'string',
        committer: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        parents: [
          'string'
        ],
        push: {
          _links: {
            links: {}
          },
          date: 'string',
          pushedBy: {
            _links: {
              links: {}
            },
            descriptor: 'string',
            displayName: 'string',
            url: 'string'
          },
          pushId: 2,
          url: 'string'
        },
        remoteUrl: 'string',
        statuses: [
          {
            _links: {
              links: {}
            },
            context: {
              genre: 'string',
              name: 'string'
            },
            createdBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            creationDate: 'string',
            description: 'string',
            id: 7,
            state: 'succeeded',
            targetUrl: 'string',
            updatedDate: 'string'
          }
        ],
        url: 'string',
        workItems: [
          {
            id: 'string',
            url: 'string'
          }
        ]
      },
      lastMergeSourceCommit: {
        _links: {
          links: {}
        },
        author: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        changeCounts: {},
        changes: [
          {
            changeType: 'branch',
            item: 'string',
            newContent: {
              content: 'string',
              contentType: 'rawText'
            },
            sourceServerItem: 'string',
            url: 'string'
          }
        ],
        comment: 'string',
        commentTruncated: false,
        commitId: 'string',
        committer: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        parents: [
          'string'
        ],
        push: {
          _links: {
            links: {}
          },
          date: 'string',
          pushedBy: {
            _links: {
              links: {}
            },
            descriptor: 'string',
            displayName: 'string',
            url: 'string'
          },
          pushId: 9,
          url: 'string'
        },
        remoteUrl: 'string',
        statuses: [
          {
            _links: {
              links: {}
            },
            context: {
              genre: 'string',
              name: 'string'
            },
            createdBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            creationDate: 'string',
            description: 'string',
            id: 5,
            state: 'pending',
            targetUrl: 'string',
            updatedDate: 'string'
          }
        ],
        url: 'string',
        workItems: [
          {
            id: 'string',
            url: 'string'
          }
        ]
      },
      lastMergeTargetCommit: {
        _links: {
          links: {}
        },
        author: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        changeCounts: {},
        changes: [
          {
            changeType: 'sourceRename',
            item: 'string',
            newContent: {
              content: 'string',
              contentType: 'rawText'
            },
            sourceServerItem: 'string',
            url: 'string'
          }
        ],
        comment: 'string',
        commentTruncated: false,
        commitId: 'string',
        committer: {
          date: 'string',
          email: 'string',
          imageUrl: 'string',
          name: 'string'
        },
        parents: [
          'string'
        ],
        push: {
          _links: {
            links: {}
          },
          date: 'string',
          pushedBy: {
            _links: {
              links: {}
            },
            descriptor: 'string',
            displayName: 'string',
            url: 'string'
          },
          pushId: 10,
          url: 'string'
        },
        remoteUrl: 'string',
        statuses: [
          {
            _links: {
              links: {}
            },
            context: {
              genre: 'string',
              name: 'string'
            },
            createdBy: {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            },
            creationDate: 'string',
            description: 'string',
            id: 1,
            state: 'notSet',
            targetUrl: 'string',
            updatedDate: 'string'
          }
        ],
        url: 'string',
        workItems: [
          {
            id: 'string',
            url: 'string'
          }
        ]
      },
      mergeFailureMessage: 'string',
      mergeFailureType: 'objectTooLarge',
      mergeId: 'string',
      mergeOptions: {
        conflictAuthorshipCommits: false,
        detectRenameFalsePositives: true,
        disableRenames: true
      },
      mergeStatus: 'conflicts',
      pullRequestId: 3,
      remoteUrl: 'string',
      repository: {
        _links: {
          links: {}
        },
        defaultBranch: 'string',
        id: 'string',
        isDisabled: false,
        isFork: false,
        name: 'string',
        parentRepository: {
          collection: {
            id: 'string',
            name: 'string',
            url: 'string'
          },
          id: 'string',
          isFork: true,
          name: 'string',
          project: {
            abbreviation: 'string',
            defaultTeamImageUrl: 'string',
            description: 'string',
            id: 'string',
            lastUpdateTime: 'string',
            name: 'string',
            revision: 3,
            state: 'createPending',
            url: 'string',
            visibility: 'private'
          },
          remoteUrl: 'string',
          sshUrl: 'string',
          url: 'string'
        },
        project: {
          abbreviation: 'string',
          defaultTeamImageUrl: 'string',
          description: 'string',
          id: 'string',
          lastUpdateTime: 'string',
          name: 'string',
          revision: 3,
          state: 'deleted',
          url: 'string',
          visibility: 'public'
        },
        remoteUrl: 'string',
        size: 3,
        sshUrl: 'string',
        url: 'string',
        validRemoteUrls: [
          'string'
        ],
        webUrl: 'string'
      },
      reviewers: [
        {
          _links: {
            links: {}
          },
          descriptor: 'string',
          displayName: 'string',
          url: 'string'
        }
      ],
      sourceRefName: 'string',
      status: 'active',
      supportsIterations: false,
      targetRefName: 'string',
      title: 'string',
      url: 'string',
      workItemRefs: [
        {
          id: 'string',
          url: 'string'
        }
      ]
    };
    describe('#pullRequestsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestsUpdate('fakedata', pullRequestsPullRequestsUpdateBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequests', 'pullRequestsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const annotatedTagsAnnotatedTagsCreateBodyParam = {
      message: 'string',
      name: 'string',
      objectId: 'string',
      taggedBy: {
        date: 'string',
        email: 'string',
        imageUrl: 'string',
        name: 'string'
      },
      taggedObject: {
        objectId: 'string',
        objectType: 'tag'
      },
      url: 'string'
    };
    describe('#annotatedTagsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.annotatedTagsCreate('fakedata', annotatedTagsAnnotatedTagsCreateBodyParam, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnnotatedTags', 'annotatedTagsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#annotatedTagsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.annotatedTagsGet('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnnotatedTags', 'annotatedTagsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const blobsBlobsGetBlobsZipBodyParam = [
      'string'
    ];
    describe('#blobsGetBlobsZip - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.blobsGetBlobsZip('fakedata', blobsBlobsGetBlobsZipBodyParam, 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Blobs', 'blobsGetBlobsZip', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#blobsGetBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.blobsGetBlob('fakedata', 'fakedata', 'fakedata', 'fakedata', null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Blobs', 'blobsGetBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cherryPicksCherryPicksCreateBodyParam = {
      generatedRefName: 'string',
      ontoRefName: 'string',
      repository: {
        _links: {
          links: {}
        },
        defaultBranch: 'string',
        id: 'string',
        isDisabled: true,
        isFork: true,
        name: 'string',
        parentRepository: {
          collection: {
            id: 'string',
            name: 'string',
            url: 'string'
          },
          id: 'string',
          isFork: false,
          name: 'string',
          project: {
            abbreviation: 'string',
            defaultTeamImageUrl: 'string',
            description: 'string',
            id: 'string',
            lastUpdateTime: 'string',
            name: 'string',
            revision: 3,
            state: 'unchanged',
            url: 'string',
            visibility: 'private'
          },
          remoteUrl: 'string',
          sshUrl: 'string',
          url: 'string'
        },
        project: {
          abbreviation: 'string',
          defaultTeamImageUrl: 'string',
          description: 'string',
          id: 'string',
          lastUpdateTime: 'string',
          name: 'string',
          revision: 4,
          state: 'deleted',
          url: 'string',
          visibility: 'private'
        },
        remoteUrl: 'string',
        size: 6,
        sshUrl: 'string',
        url: 'string',
        validRemoteUrls: [
          'string'
        ],
        webUrl: 'string'
      },
      source: {
        commitList: [
          {
            _links: {
              links: {}
            },
            author: {
              date: 'string',
              email: 'string',
              imageUrl: 'string',
              name: 'string'
            },
            changeCounts: {},
            changes: [
              {
                changeType: 'sourceRename',
                item: 'string',
                newContent: {
                  content: 'string',
                  contentType: 'rawText'
                },
                sourceServerItem: 'string',
                url: 'string'
              }
            ],
            comment: 'string',
            commentTruncated: true,
            commitId: 'string',
            committer: {
              date: 'string',
              email: 'string',
              imageUrl: 'string',
              name: 'string'
            },
            parents: [
              'string'
            ],
            push: {
              _links: {
                links: {}
              },
              date: 'string',
              pushedBy: {
                _links: {
                  links: {}
                },
                descriptor: 'string',
                displayName: 'string',
                url: 'string'
              },
              pushId: 7,
              url: 'string'
            },
            remoteUrl: 'string',
            statuses: [
              {
                _links: {
                  links: {}
                },
                context: {
                  genre: 'string',
                  name: 'string'
                },
                createdBy: {
                  _links: {
                    links: {}
                  },
                  descriptor: 'string',
                  displayName: 'string',
                  url: 'string'
                },
                creationDate: 'string',
                description: 'string',
                id: 8,
                state: 'succeeded',
                targetUrl: 'string',
                updatedDate: 'string'
              }
            ],
            url: 'string',
            workItems: [
              {
                id: 'string',
                url: 'string'
              }
            ]
          }
        ],
        pullRequestId: 3
      }
    };
    describe('#cherryPicksCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cherryPicksCreate('fakedata', cherryPicksCherryPicksCreateBodyParam, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CherryPicks', 'cherryPicksCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cherryPicksGetCherryPickForRefName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cherryPicksGetCherryPickForRefName('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CherryPicks', 'cherryPicksGetCherryPickForRefName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cherryPicksGetCherryPick - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cherryPicksGetCherryPick('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CherryPicks', 'cherryPicksGetCherryPick', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitsGetPushCommits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.commitsGetPushCommits('fakedata', 'fakedata', 555, 'fakedata', null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'commitsGetPushCommits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.commitsGet('fakedata', 'fakedata', 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'commitsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitsGetChanges - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.commitsGetChanges('fakedata', 'fakedata', 'fakedata', 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'commitsGetChanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitsGetCommits - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.commitsGetCommits('fakedata', 'fakedata', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'commitsGetCommits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commitsCommitsGetCommitsBatchBodyParam = {
      $skip: 2,
      $top: 7,
      author: 'string',
      compareVersion: {
        version: 'string',
        versionOptions: 'firstParent',
        versionType: 'commit'
      },
      excludeDeletes: true,
      fromCommitId: 'string',
      fromDate: 'string',
      historyMode: 'fullHistory',
      ids: [
        'string'
      ],
      includeLinks: false,
      includePushData: false,
      includeUserImageUrl: false,
      includeWorkItems: false,
      itemPath: 'string',
      itemVersion: {
        version: 'string',
        versionOptions: 'firstParent',
        versionType: 'commit'
      },
      showOldestCommitsFirst: true,
      toCommitId: 'string',
      toDate: 'string',
      user: 'string'
    };
    describe('#commitsGetCommitsBatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.commitsGetCommitsBatch('fakedata', commitsCommitsGetCommitsBatchBodyParam, 'fakedata', 'fakedata', null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'commitsGetCommitsBatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusesStatusesCreateBodyParam = {
      _links: {
        links: {}
      },
      context: {
        genre: 'string',
        name: 'string'
      },
      createdBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      creationDate: 'string',
      description: 'string',
      id: 1,
      state: 'pending',
      targetUrl: 'string',
      updatedDate: 'string'
    };
    describe('#statusesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.statusesCreate('fakedata', statusesStatusesCreateBodyParam, 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Statuses', 'statusesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statusesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statusesList('fakedata', 'fakedata', 'fakedata', 'fakedata', null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Statuses', 'statusesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diffsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.diffsGet('fakedata', 'fakedata', 'fakedata', null, null, null, null, null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Diffs', 'diffsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const importRequestsImportRequestsCreateBodyParam = {
      _links: {
        links: {}
      },
      detailedStatus: {
        allSteps: [
          'string'
        ],
        currentStep: 10,
        errorMessage: 'string'
      },
      importRequestId: 8,
      parameters: {
        deleteServiceEndpointAfterImportIsDone: false,
        gitSource: {
          overwrite: false,
          url: 'string'
        },
        serviceEndpointId: 'string',
        tfvcSource: {
          importHistory: false,
          importHistoryDurationInDays: 2,
          path: 'string'
        }
      },
      repository: {
        _links: {
          links: {}
        },
        defaultBranch: 'string',
        id: 'string',
        isDisabled: true,
        isFork: false,
        name: 'string',
        parentRepository: {
          collection: {
            id: 'string',
            name: 'string',
            url: 'string'
          },
          id: 'string',
          isFork: true,
          name: 'string',
          project: {
            abbreviation: 'string',
            defaultTeamImageUrl: 'string',
            description: 'string',
            id: 'string',
            lastUpdateTime: 'string',
            name: 'string',
            revision: 9,
            state: 'createPending',
            url: 'string',
            visibility: 'public'
          },
          remoteUrl: 'string',
          sshUrl: 'string',
          url: 'string'
        },
        project: {
          abbreviation: 'string',
          defaultTeamImageUrl: 'string',
          description: 'string',
          id: 'string',
          lastUpdateTime: 'string',
          name: 'string',
          revision: 4,
          state: 'all',
          url: 'string',
          visibility: 'public'
        },
        remoteUrl: 'string',
        size: 3,
        sshUrl: 'string',
        url: 'string',
        validRemoteUrls: [
          'string'
        ],
        webUrl: 'string'
      },
      status: 'queued',
      url: 'string'
    };
    describe('#importRequestsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importRequestsCreate('fakedata', importRequestsImportRequestsCreateBodyParam, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImportRequests', 'importRequestsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importRequestsQuery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importRequestsQuery('fakedata', 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImportRequests', 'importRequestsQuery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importRequestsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importRequestsGet('fakedata', 'fakedata', 'fakedata', 555, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImportRequests', 'importRequestsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const importRequestsImportRequestsUpdateBodyParam = {
      _links: {
        links: {}
      },
      detailedStatus: {
        allSteps: [
          'string'
        ],
        currentStep: 5,
        errorMessage: 'string'
      },
      importRequestId: 2,
      parameters: {
        deleteServiceEndpointAfterImportIsDone: true,
        gitSource: {
          overwrite: true,
          url: 'string'
        },
        serviceEndpointId: 'string',
        tfvcSource: {
          importHistory: true,
          importHistoryDurationInDays: 5,
          path: 'string'
        }
      },
      repository: {
        _links: {
          links: {}
        },
        defaultBranch: 'string',
        id: 'string',
        isDisabled: false,
        isFork: true,
        name: 'string',
        parentRepository: {
          collection: {
            id: 'string',
            name: 'string',
            url: 'string'
          },
          id: 'string',
          isFork: false,
          name: 'string',
          project: {
            abbreviation: 'string',
            defaultTeamImageUrl: 'string',
            description: 'string',
            id: 'string',
            lastUpdateTime: 'string',
            name: 'string',
            revision: 4,
            state: 'new',
            url: 'string',
            visibility: 'public'
          },
          remoteUrl: 'string',
          sshUrl: 'string',
          url: 'string'
        },
        project: {
          abbreviation: 'string',
          defaultTeamImageUrl: 'string',
          description: 'string',
          id: 'string',
          lastUpdateTime: 'string',
          name: 'string',
          revision: 2,
          state: 'wellFormed',
          url: 'string',
          visibility: 'public'
        },
        remoteUrl: 'string',
        size: 5,
        sshUrl: 'string',
        url: 'string',
        validRemoteUrls: [
          'string'
        ],
        webUrl: 'string'
      },
      status: 'failed',
      url: 'string'
    };
    describe('#importRequestsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importRequestsUpdate('fakedata', importRequestsImportRequestsUpdateBodyParam, 'fakedata', 'fakedata', 555, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImportRequests', 'importRequestsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#itemsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.itemsList('fakedata', 'fakedata', 'fakedata', 'fakedata', null, null, null, null, null, null, null, null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Items', 'itemsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const itemsItemsGetItemsBatchBodyParam = {
      includeContentMetadata: true,
      includeLinks: false,
      itemDescriptors: [
        {
          path: 'string',
          recursionLevel: 'full',
          version: 'string',
          versionOptions: 'none',
          versionType: 'tag'
        }
      ],
      latestProcessedChange: true
    };
    describe('#itemsGetItemsBatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.itemsGetItemsBatch('fakedata', itemsItemsGetItemsBatchBodyParam, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.value[0]));
                assert.equal(true, Array.isArray(data.response.value[1]));
                assert.equal(true, Array.isArray(data.response.value[2]));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Items', 'itemsGetItemsBatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestQueryPullRequestQueryGetBodyParam = {
      queries: [
        {
          items: [
            'string'
          ],
          type: 'lastMergeCommit'
        }
      ],
      results: [
        {}
      ]
    };
    describe('#pullRequestQueryGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestQueryGet('fakedata', pullRequestQueryPullRequestQueryGetBodyParam, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestQuery', 'pullRequestQueryGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestAttachmentsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestAttachmentsList('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestAttachments', 'pullRequestAttachmentsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestAttachmentsPullRequestAttachmentsCreateBodyParam = {};
    describe('#pullRequestAttachmentsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestAttachmentsCreate('fakedata', pullRequestAttachmentsPullRequestAttachmentsCreateBodyParam, 'fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestAttachments', 'pullRequestAttachmentsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestAttachmentsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestAttachmentsDelete('fakedata', 'fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestAttachments', 'pullRequestAttachmentsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestAttachmentsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestAttachmentsGet('fakedata', 'fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestAttachments', 'pullRequestAttachmentsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommitsGetPullRequestCommits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestCommits('fakedata', 'fakedata', 555, 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestCommits', 'pullRequestCommitsGetPullRequestCommits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommitsGetPullRequestIterationCommits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestCommitsGetPullRequestIterationCommits('fakedata', 'fakedata', 555, 555, 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestCommits', 'pullRequestCommitsGetPullRequestIterationCommits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestIterationsList('fakedata', 'fakedata', 555, 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestIterations', 'pullRequestIterationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestIterationsGet('fakedata', 'fakedata', 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestIterations', 'pullRequestIterationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationChangesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestIterationChangesGet('fakedata', 'fakedata', 555, 555, 'fakedata', null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestIterationChanges', 'pullRequestIterationChangesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestIterationStatusesPullRequestIterationStatusesCreateBodyParam = {
      _links: {
        links: {}
      },
      context: {
        genre: 'string',
        name: 'string'
      },
      createdBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      creationDate: 'string',
      description: 'string',
      id: 1,
      state: 'succeeded',
      targetUrl: 'string',
      updatedDate: 'string'
    };
    describe('#pullRequestIterationStatusesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestIterationStatusesCreate('fakedata', pullRequestIterationStatusesPullRequestIterationStatusesCreateBodyParam, 'fakedata', 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestIterationStatuses', 'pullRequestIterationStatusesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationStatusesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestIterationStatusesList('fakedata', 'fakedata', 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestIterationStatuses', 'pullRequestIterationStatusesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestIterationStatusesPullRequestIterationStatusesUpdateBodyParam = {
      0: {
        from: 'string',
        op: 'add',
        path: 'string',
        value: {}
      }
    };
    describe('#pullRequestIterationStatusesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestIterationStatusesUpdate('fakedata', pullRequestIterationStatusesPullRequestIterationStatusesUpdateBodyParam, 'fakedata', 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestIterationStatuses', 'pullRequestIterationStatusesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationStatusesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestIterationStatusesDelete('fakedata', 'fakedata', 555, 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestIterationStatuses', 'pullRequestIterationStatusesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestIterationStatusesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestIterationStatusesGet('fakedata', 'fakedata', 555, 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestIterationStatuses', 'pullRequestIterationStatusesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestLabelsPullRequestLabelsCreateBodyParam = {
      name: 'string'
    };
    describe('#pullRequestLabelsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestLabelsCreate('fakedata', pullRequestLabelsPullRequestLabelsCreateBodyParam, 'fakedata', 555, 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestLabels', 'pullRequestLabelsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestLabelsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestLabelsList('fakedata', 'fakedata', 555, 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestLabels', 'pullRequestLabelsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestLabelsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestLabelsDelete('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestLabels', 'pullRequestLabelsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestLabelsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestLabelsGet('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestLabels', 'pullRequestLabelsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestPropertiesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestPropertiesList('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestProperties', 'pullRequestPropertiesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestPropertiesPullRequestPropertiesUpdateBodyParam = {
      0: {
        from: 'string',
        op: 'replace',
        path: 'string',
        value: {}
      }
    };
    describe('#pullRequestPropertiesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestPropertiesUpdate('fakedata', pullRequestPropertiesPullRequestPropertiesUpdateBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestProperties', 'pullRequestPropertiesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestReviewersPullRequestReviewersCreatePullRequestReviewersBodyParam = [
      {}
    ];
    describe('#pullRequestReviewersCreatePullRequestReviewers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewers('fakedata', pullRequestReviewersPullRequestReviewersCreatePullRequestReviewersBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestReviewers', 'pullRequestReviewersCreatePullRequestReviewers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestReviewersPullRequestReviewersCreateUnmaterializedPullRequestReviewerBodyParam = {
      _links: {
        links: {}
      },
      descriptor: 'string',
      displayName: 'string',
      url: 'string'
    };
    describe('#pullRequestReviewersCreateUnmaterializedPullRequestReviewer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestReviewersCreateUnmaterializedPullRequestReviewer('fakedata', pullRequestReviewersPullRequestReviewersCreateUnmaterializedPullRequestReviewerBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestReviewers', 'pullRequestReviewersCreateUnmaterializedPullRequestReviewer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestReviewersList('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestReviewers', 'pullRequestReviewersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestReviewersPullRequestReviewersUpdatePullRequestReviewersBodyParam = [
      {}
    ];
    describe('#pullRequestReviewersUpdatePullRequestReviewers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewers('fakedata', pullRequestReviewersPullRequestReviewersUpdatePullRequestReviewersBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestReviewers', 'pullRequestReviewersUpdatePullRequestReviewers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestReviewersPullRequestReviewersCreatePullRequestReviewerBodyParam = {
      _links: {
        links: {}
      },
      descriptor: 'string',
      displayName: 'string',
      url: 'string'
    };
    describe('#pullRequestReviewersCreatePullRequestReviewer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestReviewersCreatePullRequestReviewer('fakedata', pullRequestReviewersPullRequestReviewersCreatePullRequestReviewerBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestReviewers', 'pullRequestReviewersCreatePullRequestReviewer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestReviewersDelete('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestReviewers', 'pullRequestReviewersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestReviewersGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestReviewersGet('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestReviewers', 'pullRequestReviewersGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestReviewersPullRequestReviewersUpdatePullRequestReviewerBodyParam = {
      _links: {
        links: {}
      },
      descriptor: 'string',
      displayName: 'string',
      url: 'string'
    };
    describe('#pullRequestReviewersUpdatePullRequestReviewer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestReviewersUpdatePullRequestReviewer('fakedata', pullRequestReviewersPullRequestReviewersUpdatePullRequestReviewerBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestReviewers', 'pullRequestReviewersUpdatePullRequestReviewer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestSharePullRequestShareSharePullRequestBodyParam = {
      message: 'string',
      receivers: [
        {
          _links: {
            links: {}
          },
          descriptor: 'string',
          displayName: 'string',
          url: 'string'
        }
      ]
    };
    describe('#pullRequestShareSharePullRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestShareSharePullRequest('fakedata', pullRequestSharePullRequestShareSharePullRequestBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestShare', 'pullRequestShareSharePullRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestStatusesPullRequestStatusesCreateBodyParam = {
      _links: {
        links: {}
      },
      context: {
        genre: 'string',
        name: 'string'
      },
      createdBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      creationDate: 'string',
      description: 'string',
      id: 9,
      state: 'notApplicable',
      targetUrl: 'string',
      updatedDate: 'string'
    };
    describe('#pullRequestStatusesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestStatusesCreate('fakedata', pullRequestStatusesPullRequestStatusesCreateBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestStatuses', 'pullRequestStatusesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestStatusesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestStatusesList('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestStatuses', 'pullRequestStatusesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestStatusesPullRequestStatusesUpdateBodyParam = {
      0: {
        from: 'string',
        op: 'remove',
        path: 'string',
        value: {}
      }
    };
    describe('#pullRequestStatusesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestStatusesUpdate('fakedata', pullRequestStatusesPullRequestStatusesUpdateBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestStatuses', 'pullRequestStatusesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestStatusesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestStatusesDelete('fakedata', 'fakedata', 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestStatuses', 'pullRequestStatusesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestStatusesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestStatusesGet('fakedata', 'fakedata', 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestStatuses', 'pullRequestStatusesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestThreadsPullRequestThreadsCreateBodyParam = {
      _links: {
        links: {}
      },
      comments: [
        {
          _links: {
            links: {}
          },
          author: {
            _links: {
              links: {}
            },
            descriptor: 'string',
            displayName: 'string',
            url: 'string'
          },
          commentType: 'codeChange',
          content: 'string',
          id: 10,
          isDeleted: false,
          lastContentUpdatedDate: 'string',
          lastUpdatedDate: 'string',
          parentCommentId: 10,
          publishedDate: 'string',
          usersLiked: [
            {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            }
          ]
        }
      ],
      id: 9,
      identities: {},
      isDeleted: true,
      lastUpdatedDate: 'string',
      properties: {
        count: 9,
        item: {},
        keys: [
          'string'
        ],
        values: [
          'string'
        ]
      },
      publishedDate: 'string',
      status: 'byDesign',
      threadContext: {
        filePath: 'string',
        leftFileEnd: {
          line: 1,
          offset: 3
        },
        leftFileStart: {
          line: 2,
          offset: 6
        },
        rightFileEnd: {
          line: 10,
          offset: 2
        },
        rightFileStart: {
          line: 3,
          offset: 10
        }
      }
    };
    describe('#pullRequestThreadsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestThreadsCreate('fakedata', pullRequestThreadsPullRequestThreadsCreateBodyParam, 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestThreads', 'pullRequestThreadsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestThreadsList('fakedata', 'fakedata', 555, 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestThreads', 'pullRequestThreadsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestThreadsGet('fakedata', 'fakedata', 555, 555, 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestThreads', 'pullRequestThreadsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestThreadsPullRequestThreadsUpdateBodyParam = {
      _links: {
        links: {}
      },
      comments: [
        {
          _links: {
            links: {}
          },
          author: {
            _links: {
              links: {}
            },
            descriptor: 'string',
            displayName: 'string',
            url: 'string'
          },
          commentType: 'codeChange',
          content: 'string',
          id: 3,
          isDeleted: false,
          lastContentUpdatedDate: 'string',
          lastUpdatedDate: 'string',
          parentCommentId: 5,
          publishedDate: 'string',
          usersLiked: [
            {
              _links: {
                links: {}
              },
              descriptor: 'string',
              displayName: 'string',
              url: 'string'
            }
          ]
        }
      ],
      id: 10,
      identities: {},
      isDeleted: false,
      lastUpdatedDate: 'string',
      properties: {
        count: 4,
        item: {},
        keys: [
          'string'
        ],
        values: [
          'string'
        ]
      },
      publishedDate: 'string',
      status: 'pending',
      threadContext: {
        filePath: 'string',
        leftFileEnd: {
          line: 7,
          offset: 6
        },
        leftFileStart: {
          line: 5,
          offset: 4
        },
        rightFileEnd: {
          line: 1,
          offset: 3
        },
        rightFileStart: {
          line: 4,
          offset: 10
        }
      }
    };
    describe('#pullRequestThreadsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestThreadsUpdate('fakedata', pullRequestThreadsPullRequestThreadsUpdateBodyParam, 'fakedata', 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestThreads', 'pullRequestThreadsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestThreadCommentsPullRequestThreadCommentsCreateBodyParam = {
      _links: {
        links: {}
      },
      author: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      commentType: 'unknown',
      content: 'string',
      id: 9,
      isDeleted: false,
      lastContentUpdatedDate: 'string',
      lastUpdatedDate: 'string',
      parentCommentId: 8,
      publishedDate: 'string',
      usersLiked: [
        {
          _links: {
            links: {}
          },
          descriptor: 'string',
          displayName: 'string',
          url: 'string'
        }
      ]
    };
    describe('#pullRequestThreadCommentsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestThreadCommentsCreate('fakedata', pullRequestThreadCommentsPullRequestThreadCommentsCreateBodyParam, 'fakedata', 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestThreadComments', 'pullRequestThreadCommentsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadCommentsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestThreadCommentsList('fakedata', 'fakedata', 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestThreadComments', 'pullRequestThreadCommentsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadCommentsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestThreadCommentsDelete('fakedata', 'fakedata', 555, 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestThreadComments', 'pullRequestThreadCommentsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestThreadCommentsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestThreadCommentsGet('fakedata', 'fakedata', 555, 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestThreadComments', 'pullRequestThreadCommentsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullRequestThreadCommentsPullRequestThreadCommentsUpdateBodyParam = {
      _links: {
        links: {}
      },
      author: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      commentType: 'text',
      content: 'string',
      id: 5,
      isDeleted: true,
      lastContentUpdatedDate: 'string',
      lastUpdatedDate: 'string',
      parentCommentId: 5,
      publishedDate: 'string',
      usersLiked: [
        {
          _links: {
            links: {}
          },
          descriptor: 'string',
          displayName: 'string',
          url: 'string'
        }
      ]
    };
    describe('#pullRequestThreadCommentsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestThreadCommentsUpdate('fakedata', pullRequestThreadCommentsPullRequestThreadCommentsUpdateBodyParam, 'fakedata', 555, 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestThreadComments', 'pullRequestThreadCommentsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommentLikesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestCommentLikesCreate('fakedata', 'fakedata', 555, 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestCommentLikes', 'pullRequestCommentLikesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommentLikesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pullRequestCommentLikesDelete('fakedata', 'fakedata', 555, 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestCommentLikes', 'pullRequestCommentLikesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestCommentLikesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestCommentLikesList('fakedata', 'fakedata', 555, 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestCommentLikes', 'pullRequestCommentLikesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pullRequestWorkItemsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pullRequestWorkItemsList('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullRequestWorkItems', 'pullRequestWorkItemsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pushesPushesCreateBodyParam = {
      _links: {
        links: {}
      },
      date: 'string',
      pushedBy: {
        _links: {
          links: {}
        },
        descriptor: 'string',
        displayName: 'string',
        url: 'string'
      },
      pushId: 7,
      url: 'string'
    };
    describe('#pushesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pushesCreate('fakedata', pushesPushesCreateBodyParam, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pushes', 'pushesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pushesList('fakedata', 'fakedata', 'fakedata', null, null, null, null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pushes', 'pushesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pushesGet('fakedata', 'fakedata', 555, 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pushes', 'pushesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.refsList('fakedata', 'fakedata', 'fakedata', null, null, null, null, null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Refs', 'refsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const refsRefsUpdateRefBodyParam = {
      isLocked: false,
      name: 'string',
      newObjectId: 'string',
      oldObjectId: 'string',
      repositoryId: 'string'
    };
    describe('#refsUpdateRef - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refsUpdateRef('fakedata', refsRefsUpdateRefBodyParam, 'fakedata', 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Refs', 'refsUpdateRef', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const refsRefsUpdateRefsBodyParam = [
      {}
    ];
    describe('#refsUpdateRefs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.refsUpdateRefs('fakedata', refsRefsUpdateRefsBodyParam, 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Refs', 'refsUpdateRefs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const revertsRevertsCreateBodyParam = {
      generatedRefName: 'string',
      ontoRefName: 'string',
      repository: {
        _links: {
          links: {}
        },
        defaultBranch: 'string',
        id: 'string',
        isDisabled: false,
        isFork: true,
        name: 'string',
        parentRepository: {
          collection: {
            id: 'string',
            name: 'string',
            url: 'string'
          },
          id: 'string',
          isFork: false,
          name: 'string',
          project: {
            abbreviation: 'string',
            defaultTeamImageUrl: 'string',
            description: 'string',
            id: 'string',
            lastUpdateTime: 'string',
            name: 'string',
            revision: 6,
            state: 'unchanged',
            url: 'string',
            visibility: 'private'
          },
          remoteUrl: 'string',
          sshUrl: 'string',
          url: 'string'
        },
        project: {
          abbreviation: 'string',
          defaultTeamImageUrl: 'string',
          description: 'string',
          id: 'string',
          lastUpdateTime: 'string',
          name: 'string',
          revision: 2,
          state: 'new',
          url: 'string',
          visibility: 'private'
        },
        remoteUrl: 'string',
        size: 3,
        sshUrl: 'string',
        url: 'string',
        validRemoteUrls: [
          'string'
        ],
        webUrl: 'string'
      },
      source: {
        commitList: [
          {
            _links: {
              links: {}
            },
            author: {
              date: 'string',
              email: 'string',
              imageUrl: 'string',
              name: 'string'
            },
            changeCounts: {},
            changes: [
              {
                changeType: 'encoding',
                item: 'string',
                newContent: {
                  content: 'string',
                  contentType: 'base64Encoded'
                },
                sourceServerItem: 'string',
                url: 'string'
              }
            ],
            comment: 'string',
            commentTruncated: true,
            commitId: 'string',
            committer: {
              date: 'string',
              email: 'string',
              imageUrl: 'string',
              name: 'string'
            },
            parents: [
              'string'
            ],
            push: {
              _links: {
                links: {}
              },
              date: 'string',
              pushedBy: {
                _links: {
                  links: {}
                },
                descriptor: 'string',
                displayName: 'string',
                url: 'string'
              },
              pushId: 2,
              url: 'string'
            },
            remoteUrl: 'string',
            statuses: [
              {
                _links: {
                  links: {}
                },
                context: {
                  genre: 'string',
                  name: 'string'
                },
                createdBy: {
                  _links: {
                    links: {}
                  },
                  descriptor: 'string',
                  displayName: 'string',
                  url: 'string'
                },
                creationDate: 'string',
                description: 'string',
                id: 7,
                state: 'failed',
                targetUrl: 'string',
                updatedDate: 'string'
              }
            ],
            url: 'string',
            workItems: [
              {
                id: 'string',
                url: 'string'
              }
            ]
          }
        ],
        pullRequestId: 1
      }
    };
    describe('#revertsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revertsCreate('fakedata', revertsRevertsCreateBodyParam, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reverts', 'revertsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revertsGetRevertForRefName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revertsGetRevertForRefName('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reverts', 'revertsGetRevertForRefName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revertsGetRevert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revertsGetRevert('fakedata', 'fakedata', 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reverts', 'revertsGetRevert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsList('fakedata', 'fakedata', 'fakedata', null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#suggestionsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.suggestionsList('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Suggestions', 'suggestionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#treesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.treesGet('fakedata', 'fakedata', 'fakedata', 'fakedata', null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Trees', 'treesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mergeBasesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.mergeBasesList('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MergeBases', 'mergeBasesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forksList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.forksList('fakedata', 'fakedata', 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Forks', 'forksList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const forksForksCreateForkSyncRequestBodyParam = {
      source: {
        collectionId: 'string',
        projectId: 'string',
        repositoryId: 'string'
      },
      sourceToTargetRefs: [
        {
          sourceRef: 'string',
          targetRef: 'string'
        }
      ]
    };
    describe('#forksCreateForkSyncRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.forksCreateForkSyncRequest('fakedata', forksForksCreateForkSyncRequestBodyParam, 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Forks', 'forksCreateForkSyncRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forksGetForkSyncRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.forksGetForkSyncRequests('fakedata', 'fakedata', 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Forks', 'forksGetForkSyncRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forksGetForkSyncRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.forksGetForkSyncRequest('fakedata', 'fakedata', 555, 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Forks', 'forksGetForkSyncRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mergesMergesCreateBodyParam = {
      comment: 'string',
      parents: [
        'string'
      ]
    };
    describe('#mergesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.mergesCreate('fakedata', mergesMergesCreateBodyParam, 'fakedata', 'fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Merges', 'mergesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mergesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.mergesGet('fakedata', 'fakedata', 'fakedata', 555, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-azure_devops-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Merges', 'mergesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
