
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_20:14PM

See merge request itentialopensource/adapters/adapter-azure_devops!22

---

## 0.3.3 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-azure_devops!20

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:23PM

See merge request itentialopensource/adapters/adapter-azure_devops!19

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:36PM

See merge request itentialopensource/adapters/adapter-azure_devops!18

---

## 0.3.0 [05-08-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!17

---

## 0.2.4 [03-26-2024]

* Changes made at 2024.03.26_14:22PM

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!16

---

## 0.2.3 [03-11-2024]

* Changes made at 2024.03.11_13:21PM

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!15

---

## 0.2.2 [02-26-2024]

* Changes made at 2024.02.26_13:45PM

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!14

---

## 0.2.1 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!13

---

## 0.2.0 [11-10-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!12

---

## 0.1.11 [06-06-2023]

* fixed itemsList query params

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!11

---

## 0.1.10 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!10

---

## 0.1.9 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!10

---

## 0.1.8 [01-26-2023]

* Updated schema, inputs and removed adapter_modifications folder

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!8

---

## 0.1.7 [01-26-2023]

* ADAPT-2530 - Change body format

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!7

---

## 0.1.6 [12-01-2022]

* ADAPT-2476

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!6

---

## 0.1.5 [11-22-2022]

* Implement API request Commits – Get Commits

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!5

---

## 0.1.4 [11-22-2022]

* Query parameter across all apis showing as “apiVersion” instead of api-version

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!4

---

## 0.1.3 [10-31-2022]

* Azure DevOps Adapter Mixed Path Variables

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!3

---

## 0.1.2 [05-25-2022]

* Added Git API

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!1

---

## 0.1.1 [12-13-2021]

- Initial Commit

See commit b5990f7

---
