## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Azure Devops. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Azure Devops.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Microsoft Azure DevOps. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">pipelinesCreate(organization, body, project, apiVersion, callback)</td>
    <td style="padding:15px">Create a pipeline.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pipelinesList(organization, project, orderBy, top, continuationToken, apiVersion, callback)</td>
    <td style="padding:15px">Get a list of pipelines.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pipelinesGet(organization, project, pipelineId, pipelineVersion, apiVersion, callback)</td>
    <td style="padding:15px">Gets a pipeline, optionally at the specified version</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">previewPreview(organization, body, project, pipelineId, pipelineVersion, apiVersion, callback)</td>
    <td style="padding:15px">Queues a dry run of the pipeline and returns an object containing the final yaml.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines/{pathv3}/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runsList(organization, project, pipelineId, apiVersion, callback)</td>
    <td style="padding:15px">Gets top 10000 runs for a particular pipeline.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines/{pathv3}/runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runsRunPipeline(organization, body, project, pipelineId, pipelineVersion, apiVersion, callback)</td>
    <td style="padding:15px">Runs a pipeline.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines/{pathv3}/runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runsGet(organization, project, pipelineId, runId, apiVersion, callback)</td>
    <td style="padding:15px">Gets a run for a particular pipeline.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines/{pathv3}/runs/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">artifactsGet(organization, project, pipelineId, runId, artifactName, expand = 'none', apiVersion, callback)</td>
    <td style="padding:15px">Get a specific artifact from a pipeline run</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines/{pathv3}/runs/{pathv4}/artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logsList(organization, project, pipelineId, runId, expand = 'none', apiVersion, callback)</td>
    <td style="padding:15px">Get a list of logs from a pipeline run.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines/{pathv3}/runs/{pathv4}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logsGet(organization, project, pipelineId, runId, logId, expand = 'none', apiVersion, callback)</td>
    <td style="padding:15px">Get a specific log from a pipeline run</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/pipelines/{pathv3}/runs/{pathv4}/logs/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repositoriesGetDeletedRepositories(organization, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve deleted git repositories.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/deletedrepositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repositoriesGetRecycleBinRepositories(organization, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve soft-deleted git repositories from the recycle bin.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/recycleBin/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repositoriesDeleteRepositoryFromRecycleBin(organization, project, repositoryId, apiVersion, callback)</td>
    <td style="padding:15px">Destroy (hard delete) a soft-deleted Git repository.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/recycleBin/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repositoriesRestoreRepositoryFromRecycleBin(organization, body, project, repositoryId, apiVersion, callback)</td>
    <td style="padding:15px">Recover a soft-deleted Git repository. Recently deleted repositories go into a soft-delete state fo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/recycleBin/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repositoriesCreate(organization, body, project, sourceRef, apiVersion, callback)</td>
    <td style="padding:15px">Create a git repository in a team project.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repositoriesList(organization, project, includeLinks, includeAllUrls, includeHidden, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve git repositories.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repositoriesDelete(organization, repositoryId, project, apiVersion, callback)</td>
    <td style="padding:15px">Delete a git repository</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repositoriesGetRepository(organization, repositoryId, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a git repository.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repositoriesUpdate(organization, body, repositoryId, project, apiVersion, callback)</td>
    <td style="padding:15px">Updates the Git repository with either a new repo name or a new default branch.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refsFavoritesCreate(organization, body, project, apiVersion, callback)</td>
    <td style="padding:15px">Creates a ref favorite</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/favorites/refs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refsFavoritesList(organization, project, repositoryId, identityId, apiVersion, callback)</td>
    <td style="padding:15px">Gets the refs favorites for a repo and an identity.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/favorites/refs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refsFavoritesDelete(organization, project, favoriteId, apiVersion, callback)</td>
    <td style="padding:15px">Deletes the refs favorite specified</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/favorites/refs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refsFavoritesGet(organization, project, favoriteId, apiVersion, callback)</td>
    <td style="padding:15px">Gets the refs favorite for a favorite Id.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/favorites/refs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyConfigurationsGet(organization, project, repositoryId, refName, policyType, top, continuationToken, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a list of policy configurations by a given set of scope/filtering criteria.

Below is a sh</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/policy/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestsGetPullRequestsByProject(organization, project, searchCriteriaCreatorId, searchCriteriaIncludeLinks, searchCriteriaRepositoryId, searchCriteriaReviewerId, searchCriteriaSourceRefName, searchCriteriaSourceRepositoryId, searchCriteriaStatus = 'notSet', searchCriteriaTargetRefName, maxCommentLength, skip, top, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve all pull requests matching a specified criteria.

Please note that description field will</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/pullrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestsGetPullRequestById(organization, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/pullrequests/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestsCreate(organization, body, repositoryId, project, supportsIterations, apiVersion, callback)</td>
    <td style="padding:15px">Create a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestsGetPullRequests(organization, repositoryId, project, searchCriteriaCreatorId, searchCriteriaIncludeLinks, searchCriteriaRepositoryId, searchCriteriaReviewerId, searchCriteriaSourceRefName, searchCriteriaSourceRepositoryId, searchCriteriaStatus = 'notSet', searchCriteriaTargetRefName, maxCommentLength, skip, top, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve all pull requests matching a specified criteria.

Please note that description field will</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestsGetPullRequest(organization, repositoryId, pullRequestId, project, maxCommentLength, skip, top, includeCommits, includeWorkItemRefs, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullrequests/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestsUpdate(organization, body, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Update a pull request

These are the properties that can be updated with the API:
 - Status
 - Titl</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullrequests/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">annotatedTagsCreate(organization, body, project, repositoryId, apiVersion, callback)</td>
    <td style="padding:15px">Create an annotated tag.

Repositories have both a name and an identifier. Identifiers are globally</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/annotatedtags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">annotatedTagsGet(organization, project, repositoryId, objectId, apiVersion, callback)</td>
    <td style="padding:15px">Get an annotated tag.

Repositories have both a name and an identifier. Identifiers are globally un</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/annotatedtags/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">blobsGetBlobsZip(organization, body, repositoryId, project, filename, apiVersion, callback)</td>
    <td style="padding:15px">Gets one or more blobs in a zip file download.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/blobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">blobsGetBlob(organization, repositoryId, sha1, project, download, fileName, format, resolveLfs, apiVersion, callback)</td>
    <td style="padding:15px">Get a single blob.

Repositories have both a name and an identifier. Identifiers are globally uniqu</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/blobs/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cherryPicksCreate(organization, body, project, repositoryId, apiVersion, callback)</td>
    <td style="padding:15px">Cherry pick a specific commit or commits that are associated to a pull request into a new branch.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/cherryPicks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cherryPicksGetCherryPickForRefName(organization, project, repositoryId, refName, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve information about a cherry pick operation for a specific branch. This operation is expensi</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/cherryPicks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cherryPicksGetCherryPick(organization, project, cherryPickId, repositoryId, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve information about a cherry pick operation by cherry pick Id.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/cherryPicks/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">commitsGetPushCommits(organization, repositoryId, pushId, project, top, skip, includeLinks, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a list of commits associated with a particular push.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">commitsGet(organization, commitId, repositoryId, project, changeCount, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a particular commit.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/commits/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">commitsGetChanges(organization, commitId, repositoryId, project, top, skip, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve changes for a particular commit.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/commits/{pathv4}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">commitsGetCommits(organization, repositoryId, project, skip, top, searchCriteriaAuthor, searchCriteriaCompareVersionVersion, searchCriteriaCompareVersionVersionOptions, searchCriteriaCompareVersionVersionType, searchCriteriaExcludeDeletes, searchCriteriaFromCommitId, searchCriteriaFromDate, searchCriteriaHistoryMode, searchCriteriaIds, searchCriteriaIncludeLinks, searchCriteriaIncludePushData, searchCriteriaIncludeUserImageUrl, searchCriteriaIncludeWorkItems, searchCriteriaItemPath, searchCriteriaItemVersionVersion, searchCriteriaItemVersionVersionOptions, searchCriteriaItemVersionVersionType, searchCriteriaShowOldestCommitsFirst, searchCriteriaToCommitId, searchCriteriaToDate, searchCriteriaUser, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve git commits for a project.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">commitsGetCommitsBatch(organization, body, repositoryId, project, skip, top, includeStatuses, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve git commits for a project matching the search criteria</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/commitsbatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statusesCreate(organization, body, commitId, repositoryId, project, apiVersion, callback)</td>
    <td style="padding:15px">Create Git commit status.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/commits/{pathv4}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statusesList(organization, commitId, repositoryId, project, top, skip, latestOnly, apiVersion, callback)</td>
    <td style="padding:15px">Get statuses associated with the Git commit.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/commits/{pathv4}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diffsGet(organization, repositoryId, project, diffCommonCommit, top, skip, baseVersion, baseVersionOptions = 'none', baseVersionType = 'branch', targetVersion, targetVersionOptions = 'none', targetVersionType = 'branch', apiVersion, callback)</td>
    <td style="padding:15px">Find the closest common commit (the merge base) between base and target commits, and get the diff b</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/diffs/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importRequestsCreate(organization, body, project, repositoryId, apiVersion, callback)</td>
    <td style="padding:15px">Create an import request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/importRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importRequestsQuery(organization, project, repositoryId, includeAbandoned, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve import requests for a repository.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/importRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importRequestsGet(organization, project, repositoryId, importRequestId, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a particular import request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/importRequests/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importRequestsUpdate(organization, body, project, repositoryId, importRequestId, apiVersion, callback)</td>
    <td style="padding:15px">Retry or abandon a failed import request.

There can only be one active import request associated w</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/importRequests/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">itemsList(organization, repositoryId, project, itemPath, scopePath, recursionLevel = 'none', includeContentMetadata, latestProcessedChange, download, includeLinks, includeContent, resolveLfs, format, versionDescriptorVersion, versionDescriptorVersionOptions = 'none', versionDescriptorVersionType = 'branch', apiVersion, callback)</td>
    <td style="padding:15px">Get Item Metadata and/or Content for a collection of items. The download parameter is to indicate w</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">itemsGetItemsBatch(organization, body, repositoryId, project, apiVersion, callback)</td>
    <td style="padding:15px">Post for retrieving a creating a batch out of a set of items in a repo / project given a list of pa</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/itemsbatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestQueryGet(organization, body, repositoryId, project, apiVersion, callback)</td>
    <td style="padding:15px">This API is used to find what pull requests are related to a given commit.  It can be used to eithe</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullrequestquery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestAttachmentsList(organization, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Get a list of files attached to a given pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestAttachmentsCreate(organization, body, fileName, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Attach a new file to a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/attachments/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestAttachmentsDelete(organization, fileName, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Delete a pull request attachment.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/attachments/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestAttachmentsGet(organization, fileName, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Get the file content of a pull request attachment.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/attachments/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestCommitsGetPullRequestCommits(organization, repositoryId, pullRequestId, project, top, continuationToken, apiVersion, callback)</td>
    <td style="padding:15px">Get the commits for the specified pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestCommitsGetPullRequestIterationCommits(organization, repositoryId, pullRequestId, iterationId, project, top, skip, apiVersion, callback)</td>
    <td style="padding:15px">Get the commits for the specified iteration of a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/iterations/{pathv5}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestIterationsList(organization, repositoryId, pullRequestId, project, includeCommits, apiVersion, callback)</td>
    <td style="padding:15px">Get the list of iterations for the specified pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/iterations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestIterationsGet(organization, repositoryId, pullRequestId, iterationId, project, apiVersion, callback)</td>
    <td style="padding:15px">Get the specified iteration for a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/iterations/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestIterationChangesGet(organization, repositoryId, pullRequestId, iterationId, project, top, skip, compareTo, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve the changes made in a pull request between two iterations.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/iterations/{pathv5}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestIterationStatusesCreate(organization, body, repositoryId, pullRequestId, iterationId, project, apiVersion, callback)</td>
    <td style="padding:15px">Create a pull request status on the iteration. This operation will have the same result as Create s</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/iterations/{pathv5}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestIterationStatusesList(organization, repositoryId, pullRequestId, iterationId, project, apiVersion, callback)</td>
    <td style="padding:15px">Get all the statuses associated with a pull request iteration.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/iterations/{pathv5}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestIterationStatusesUpdate(organization, body, repositoryId, pullRequestId, iterationId, project, apiVersion, callback)</td>
    <td style="padding:15px">Update pull request iteration statuses collection. The only supported operation type is `remove`.

</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/iterations/{pathv5}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestIterationStatusesDelete(organization, repositoryId, pullRequestId, iterationId, statusId, project, apiVersion, callback)</td>
    <td style="padding:15px">Delete pull request iteration status.

You can remove multiple statuses in one call by using Update</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/iterations/{pathv5}/statuses/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestIterationStatusesGet(organization, repositoryId, pullRequestId, iterationId, statusId, project, apiVersion, callback)</td>
    <td style="padding:15px">Get the specific pull request iteration status by ID. The status ID is unique within the pull reque</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/iterations/{pathv5}/statuses/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestLabelsCreate(organization, body, repositoryId, pullRequestId, project, projectId, apiVersion, callback)</td>
    <td style="padding:15px">Create a label for a specified pull request. The only required field is the name of the new label.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestLabelsList(organization, repositoryId, pullRequestId, project, projectId, apiVersion, callback)</td>
    <td style="padding:15px">Get all the labels assigned to a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestLabelsDelete(organization, repositoryId, pullRequestId, labelIdOrName, project, projectId, apiVersion, callback)</td>
    <td style="padding:15px">Removes a label from the set of those assigned to the pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/labels/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestLabelsGet(organization, repositoryId, pullRequestId, labelIdOrName, project, projectId, apiVersion, callback)</td>
    <td style="padding:15px">Retrieves a single label that has been assigned to a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/labels/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestPropertiesList(organization, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Get external properties of the pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestPropertiesUpdate(organization, body, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Create or update pull request external properties. The patch operation can be `add`, `replace` or `</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestReviewersCreatePullRequestReviewers(organization, body, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Add reviewers to a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/reviewers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestReviewersCreateUnmaterializedPullRequestReviewer(organization, body, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Add an unmaterialized identity to the reviewers of a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/reviewers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestReviewersList(organization, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve the reviewers for a pull request</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/reviewers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestReviewersUpdatePullRequestReviewers(organization, body, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Reset the votes of multiple reviewers on a pull request.  NOTE: This endpoint only supports updatin</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/reviewers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestReviewersCreatePullRequestReviewer(organization, body, repositoryId, pullRequestId, reviewerId, project, apiVersion, callback)</td>
    <td style="padding:15px">Add a reviewer to a pull request or cast a vote.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/reviewers/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestReviewersDelete(organization, repositoryId, pullRequestId, reviewerId, project, apiVersion, callback)</td>
    <td style="padding:15px">Remove a reviewer from a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/reviewers/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestReviewersGet(organization, repositoryId, pullRequestId, reviewerId, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve information about a particular reviewer on a pull request</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/reviewers/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestReviewersUpdatePullRequestReviewer(organization, body, repositoryId, pullRequestId, reviewerId, project, apiVersion, callback)</td>
    <td style="padding:15px">Edit a reviewer entry. These fields are patchable: isFlagged, hasDeclined</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/reviewers/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestShareSharePullRequest(organization, body, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Sends an e-mail notification about a specific pull request to a set of recipients</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/share?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestStatusesCreate(organization, body, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Create a pull request status.

The only required field for the status is `Context.Name` that unique</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestStatusesList(organization, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Get all the statuses associated with a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestStatusesUpdate(organization, body, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Update pull request statuses collection. The only supported operation type is `remove`.

This opera</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestStatusesDelete(organization, repositoryId, pullRequestId, statusId, project, apiVersion, callback)</td>
    <td style="padding:15px">Delete pull request status.

You can remove multiple statuses in one call by using Update operation.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/statuses/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestStatusesGet(organization, repositoryId, pullRequestId, statusId, project, apiVersion, callback)</td>
    <td style="padding:15px">Get the specific pull request status by ID. The status ID is unique within the pull request across</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/statuses/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestThreadsCreate(organization, body, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Create a thread in a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestThreadsList(organization, repositoryId, pullRequestId, project, iteration, baseIteration, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve all threads in a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestThreadsGet(organization, repositoryId, pullRequestId, threadId, project, iteration, baseIteration, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a thread in a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestThreadsUpdate(organization, body, repositoryId, pullRequestId, threadId, project, apiVersion, callback)</td>
    <td style="padding:15px">Update a thread in a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestThreadCommentsCreate(organization, body, repositoryId, pullRequestId, threadId, project, apiVersion, callback)</td>
    <td style="padding:15px">Create a comment on a specific thread in a pull request (up to 500 comments can be created per thre</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestThreadCommentsList(organization, repositoryId, pullRequestId, threadId, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve all comments associated with a specific thread in a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestThreadCommentsDelete(organization, repositoryId, pullRequestId, threadId, commentId, project, apiVersion, callback)</td>
    <td style="padding:15px">Delete a comment associated with a specific thread in a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}/comments/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestThreadCommentsGet(organization, repositoryId, pullRequestId, threadId, commentId, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a comment associated with a specific thread in a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}/comments/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestThreadCommentsUpdate(organization, body, repositoryId, pullRequestId, threadId, commentId, project, apiVersion, callback)</td>
    <td style="padding:15px">Update a comment associated with a specific thread in a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}/comments/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestCommentLikesCreate(organization, repositoryId, pullRequestId, threadId, commentId, project, apiVersion, callback)</td>
    <td style="padding:15px">Add a like on a comment.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}/comments/{pathv6}/likes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestCommentLikesDelete(organization, repositoryId, pullRequestId, threadId, commentId, project, apiVersion, callback)</td>
    <td style="padding:15px">Delete a like on a comment.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}/comments/{pathv6}/likes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestCommentLikesList(organization, repositoryId, pullRequestId, threadId, commentId, project, apiVersion, callback)</td>
    <td style="padding:15px">Get likes for a comment.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/threads/{pathv5}/comments/{pathv6}/likes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pullRequestWorkItemsList(organization, repositoryId, pullRequestId, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a list of work items associated with a pull request.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pullRequests/{pathv4}/workitems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pushesCreate(organization, body, repositoryId, project, apiVersion, callback)</td>
    <td style="padding:15px">Push changes to the repository.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pushes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pushesList(organization, repositoryId, project, skip, top, searchCriteriaFromDate, searchCriteriaIncludeLinks, searchCriteriaIncludeRefUpdates, searchCriteriaPusherId, searchCriteriaRefName, searchCriteriaToDate, apiVersion, callback)</td>
    <td style="padding:15px">Retrieves pushes associated with the specified repository.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pushes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pushesGet(organization, repositoryId, pushId, project, includeCommits, includeRefUpdates, apiVersion, callback)</td>
    <td style="padding:15px">Retrieves a particular push.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/pushes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refsList(organization, repositoryId, project, filter, includeLinks, includeStatuses, includeMyBranches, latestStatusesOnly, peelTags, filterContains, top, continuationToken, apiVersion, callback)</td>
    <td style="padding:15px">Queries the provided repository for its refs and returns them.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/refs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refsUpdateRef(organization, body, repositoryId, filter, project, projectId, apiVersion, callback)</td>
    <td style="padding:15px">Lock or Unlock a branch.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/refs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refsUpdateRefs(organization, body, repositoryId, project, projectId, apiVersion, callback)</td>
    <td style="padding:15px">Creating, updating, or deleting refs(branches).

Updating a ref means making it point at a differen</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/refs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revertsCreate(organization, body, project, repositoryId, apiVersion, callback)</td>
    <td style="padding:15px">Starts the operation to create a new branch which reverts changes introduced by either a specific c</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/reverts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revertsGetRevertForRefName(organization, project, repositoryId, refName, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve information about a revert operation for a specific branch.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/reverts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revertsGetRevert(organization, project, revertId, repositoryId, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve information about a revert operation by revert Id.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/reverts/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsList(organization, repositoryId, project, baseVersionDescriptorVersion, baseVersionDescriptorVersionOptions = 'none', baseVersionDescriptorVersionType = 'branch', apiVersion, callback)</td>
    <td style="padding:15px">Retrieve statistics about all branches within a repository.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/stats/branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">suggestionsList(organization, repositoryId, project, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve a pull request suggestion for a particular repository or team project.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">treesGet(organization, repositoryId, sha1, project, projectId, recursive, fileName, format, apiVersion, callback)</td>
    <td style="padding:15px">The Tree endpoint returns the collection of objects underneath the specified tree. Trees are folder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/trees/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergeBasesList(organization, repositoryNameOrId, commitId, otherCommitId, project, otherCollectionId, otherRepositoryId, apiVersion, callback)</td>
    <td style="padding:15px">Find the merge bases of two commits, optionally across forks. If otherRepositoryId is not specified</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/commits/{pathv4}/mergebases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forksList(organization, repositoryNameOrId, collectionId, project, includeLinks, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve all forks of a repository in the collection.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/forks/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forksCreateForkSyncRequest(organization, body, repositoryNameOrId, project, includeLinks, apiVersion, callback)</td>
    <td style="padding:15px">Request that another repository's refs be fetched into this one. It syncs two existing forks. To cr</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/forkSyncRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forksGetForkSyncRequests(organization, repositoryNameOrId, project, includeAbandoned, includeLinks, apiVersion, callback)</td>
    <td style="padding:15px">Retrieve all requested fork sync operations on this repository.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/forkSyncRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forksGetForkSyncRequest(organization, repositoryNameOrId, forkSyncOperationId, project, includeLinks, apiVersion, callback)</td>
    <td style="padding:15px">Get a specific fork sync operation's details.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/forkSyncRequests/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergesCreate(organization, body, project, repositoryNameOrId, includeLinks, apiVersion, callback)</td>
    <td style="padding:15px">Request a git merge operation. Currently we support merging only 2 commits.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/merges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergesGet(organization, project, repositoryNameOrId, mergeOperationId, includeLinks, apiVersion, callback)</td>
    <td style="padding:15px">Get a specific merge operation's details.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/_apis/git/repositories/{pathv3}/merges/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
