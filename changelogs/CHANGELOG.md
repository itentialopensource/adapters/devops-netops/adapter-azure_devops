
## 0.1.11 [06-06-2023]

* fixed itemsList query params

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!11

---

## 0.1.10 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!10

---

## 0.1.9 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!10

---

## 0.1.8 [01-26-2023]

* Updated schema, inputs and removed adapter_modifications folder

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!8

---

## 0.1.7 [01-26-2023]

* ADAPT-2530 - Change body format

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!7

---

## 0.1.6 [12-01-2022]

* ADAPT-2476

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!6

---

## 0.1.5 [11-22-2022]

* Implement API request Commits – Get Commits

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!5

---

## 0.1.4 [11-22-2022]

* Query parameter across all apis showing as “apiVersion” instead of api-version

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!4

---

## 0.1.3 [10-31-2022]

* Azure DevOps Adapter Mixed Path Variables

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!3

---

## 0.1.2 [05-25-2022]

* Added Git API

See merge request itentialopensource/adapters/devops-netops/adapter-azure_devops!1

---

## 0.1.1 [12-13-2021]

- Initial Commit

See commit b5990f7

---
